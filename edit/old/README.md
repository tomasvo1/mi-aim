# Editace obrazu
EDIT: V neděli ráno, přes noc jsem si uvědomil, že na gpu potřebuju naimplementovat jacobi a ne gauss-seidel metodu, jinak nedávalo smysl, tak jsem úkol lehce přepracoval dole jsou některé ze starších výstupů.

Krátce popíši, jak jsem řešil pátou úlohu, ukázky obsahují i nepovedené výstupy (kterých byla kupa).

<img src="bonsmol.png" width="400">

## Postup, implementace
Zde jsem byl ještě plný entusiasmu, netušil jsem co teprve přijde.

Implementace funkcionality se nachází tentokrát na více místech - [edit.cpp](../src/edit.cpp), [gl.cpp](../src/gl.cpp) a [edit.glsl](../src/edit.glsl). Metoda Gauss-Seidel konvergovala poměrně pomalu, chv9li jsem v8hal nad multi-resolution scheme, ale nakonec mi přišlo rzchlejší zrychlit výpočet pomocí implementace na GPU v compute shaderu (viz odkazovaný shader dříve). 

Postup jsem zvolil následující:

1. získám laplacian pro oba dva obrázky (na CPU)
2. upravím okrajové podmínky dle požadavku na vstupu (na vstupu se krom obrázků čeká čtveřice XYZW, která říká co s případnými okraji udělat, pokud je vyžadováno, pak se od laplacianu (pravá strana rovnice) odečte hodnota pixelu některého z obrázků (okrajové podmínky)) (též CPU)
3. spustím gauss-seidel, buďto na CPU (nyní zakomentováno), nebo na GPU, kde se každých 1000 iterací dívám na změny oproti původnímu obrázku, beru v potaz maximální změnu
4. GS je ukončen, pokud *maximální změna oproti minulému snímku při poslední kontrole byla menší než 0.5* (vzhledem k rozsahu 0-255), nebo bylo provedeno více než 500000 iterací (na CPU tak max 10000). (test na CPU každých 1000 iterací)

Ovládání vypadá následovně:

```
open hand.jpg #načte se první obrázek, základ, base
open eye.jpg #načte se druhý obrázek, added

merge 950 550 AAAA # první dva parametry odpovídají posunu přidaného obrázku vůči prvnímu
                   # následuje čtveřice XYZW kde
                   # X - top, Y - right, Z - bottom, W - left
                   # je instrukce pro zakotvení hodnot 
                   # base obrázku (B) nebo přidaného (A) na okrajích editované zóny, nebo se nic nemění (0)
save png out

```


## Ukázky

Mezi první pokusy patřila (poměrně neoriginálně) replikace ukázky z [wikipedie](https://en.wikipedia.org/wiki/Gradient-domain_image_processing).

<img src="hand.jpg" height="150">
<img src="eye.jpg" height="150">

Levý snímek je kontrolní výstup po 100 iteracích, vpravo finální výsledek:

    merge 950 550 AAAA #zde se nenastavují okrajové podmínky, neboť editovaná oblast nesahá až do kraje...

<img src="100it.png" height="150">
<img src="handOut.png" height="150">

### Bonsaj

To se záhy podařilo, a tak jsem se přesunul k vlastní tvorbě. Jedním z mnoha dalších pokusů byla bonsaj, níže spojená ukázka

<img src="closeL.jpeg" height="400">
<img src="right.jpeg" height="400">

kde jsem poměrně rychle zjistil, že u fotografií s ostrými přechody budou problémy s nenavazujícími hranami. Jendak jsem chvíli bojoval s nastavením hraničních podmínek, osvědčilo se vpravo zakotvit hodnoty práveho obrázku a jinak nechat okraje volné, pravděpodobně by šlo ale ještě přechod zlepšit:

Vlevo nevhodné, vpravo správné okrajové podmínky:

    merge 465 0 0A0B #the right one

<img src="closeLR2.png" height="400">
<img src="close.png" height="400">

### Pokoj

Poměrně přímočarý by postup při tvrobě úvodního snímku, ale i ten se neobešel bez zádrhelů, opět jsem na počátku nevohdně nastavil okrajové podmínky, nakonec jsem ponechal všechny volné (což dává smysl, neboť obrázky mají podobnou expozici):

<img src="bon_left.jpeg" height="400">
<img src="bon_right.jpeg" height="400">

Vlevo nevhodné, vpravo správné okrajové podmínky:

    merge 0 2080 000A  #the right one, A je pro levý okraj pravé půlky, nejedná se o okraj, okraje jsou 0

<img src="bon1.png" width="400">
<img src="bonsmol.png" width="400">

### Jiné, méně povedené

Během práce jsem vyplodil i několik méně povedených ukázek, v zásadě ale vždy šlo o problémy s návazností snímků + nastavení okrajů:

Vstup:

<img src="velMid.jpeg" width="400">
<img src="velSecond.jpeg" width="400">

V následujících snímcích byl použit pouze jiný výřez z druhého snímku výše, výstupy:

<img src="vel.png" width="400">
<img src="vel1.png" width="400">
<img src="vel2.png" width="400">
<img src="vel3.png" width="400">

Stejně tak i poslední ukázka:

Vstup:

<img src="cleftOriginal.jpeg" width="400">
<img src="crightOriginal.jpeg" width="400">

a jejich kombinace, kde značně pokulhávají okraje, výstup:

<img src="comp.png" width="400">