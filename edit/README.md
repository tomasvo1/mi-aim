# Editace obrazu
EDIT: V neděli ráno, přes noc, jsem si uvědomil, že na gpu potřebuju naimplementovat jacobi a ne gauss-seidel metodu, jinak by můj původní postup nedával smysl, tak jsem úkol lehce přepracoval, zde je výsledek.

Krátce popíši, jak jsem řešil pátou úlohu, ukázky obsahují i nepovedené výstupy (kterých byla kupa).

<img src="bon.png" width="400">

## Postup, implementace
Zde jsem byl ještě plný entusiasmu, netušil jsem co teprve přijde.

Implementace funkcionality se nachází tentokrát na více místech - [edit.cpp](../src/edit.cpp), [gl.cpp](../src/gl.cpp) a [edit.glsl](../src/edit.glsl). Metoda Gauss-Seidel (dále *GS*) konvergovala poměrně pomalu, resp. výpočet poměrně trval, chvíli jsem váhal nad tím, zde implementovat *multi-resolution scheme*, ale nakonec mi přišlo rychlejší zrychlit výpočet pomocí implementace na GPU v compute shaderu (viz odkazovaný shader dříve). Zde jsem nejprve udělal dost zásadní chybu, a sice jsem si neuvědomil, že GS vlastně nejde jen tak jednoduše paralelizovat. Nakonec je pro GPU použita Jacobiova metoda (dále JM), s tím že jsem nucen použít dva buffery pro vstup a výstup, oproti GS, kde by stačil pouze jeden. 

Jelikož bylo ale v zadání napsáno že máme použít GS, implementoval jsem také GS pro CPU a na závěr jsou přiloženy porovnání výsledků obou metod, které jsou velmi podobné (ne-li shodné). Až na ukázku *ruky* byly všechny ukázky vyrobeny pomocí JM na GPU, jelikož to bylo jednoduše rychlejší. V souboru [edit.cpp](../src/edit.cpp) je define kterým se mezi implementacemi přepíná.

Postup jsem zvolil následující:

0. stranou si připravím dva obrázky, do obrazu *base* se bude vkládat, obraz *added* se vkládá a na vstupu je již ořezaná část obrázku, pro úplnost je u ukázek vždy celý i ořezaný snímek
1. získám laplacian pro oba dva obrázky (na CPU)
2. upravím okrajové podmínky dle požadavku na vstupu (na vstupu se krom obrázků čeká čtveřice XYZW, která říká co s případnými okraji udělat, pokud je vyžadováno, pak se laplacian (pravá strana rovnice) upraví dle hodnoty pixelu některého z obrázků (okrajové podmínky)) (též CPU)
3. spustím GS, buďto na CPU (nyní zakomentováno), nebo JM na GPU, kde se každých 1000 iterací dívám na změny oproti původnímu obrázku, beru v potaz maximální změnu
4. GS je ukončen, pokud *maximální změna oproti minulému snímku při poslední kontrole byla menší než 1.0* (vzhledem k rozsahu 0-255), nebo bylo provedeno více než max dovolený počet iterací (dáno vstupním parametrem), konvergenci by šlo jistě testovat daleko lépe, ale omezení max. počtu iterací a sledování rozdílu fungovalo poměrně dobře

Ovládání vypadá následovně:

```
open hand.jpg #načte se první obrázek, základ, base
open eye.jpg #načte se druhý obrázek, už musí být ořezaný, added

#
# merge x y XYZW max
#
# první dva parametry x, y odpovídají posunu přidaného obrázku vůči prvnímu
# následuje čtveřice XYZW kde
# X - top, Y - right, Z - bottom, W - left
# je instrukce pro zakotvení hodnot base obrázku (B) 
# nebo přidaného (A) na okrajích editované zóny, 
# nebo se nic nemění (0), defacto ale (0) odpovídá (B)
#

merge 950 550 AAAA 5000 

save png out

```

např. tedy  `merge 440 100 B0BA 50000` posune druhý obrázek v směru X o 440, ve směru Y o 100 a na okrajích se nahoře a dole zakotví hodnoty prvního obrázku, vlevo se zakotví hodnoty druhého obrázku a vpravo se ponechají výchozí hodnoty, maximálně bude provedeno 50000 iterací 

S okraji jsem měl nějakou dobu problém, ale finální řešení funguje myslím obstojně, myšlenka byla na začátku si jednou spočítat `b` a to pak jednduše předohdit GS nebo JM.

1. Nejprve se spočítám laplacian obou obrázků, ale *neberu v potaz okraje*. Efektivně to v tuto chvíli vypadá, jako kdyby se po okrajích opakovaly hraniční pixely. 
2. Následně spojím oba laplaciany.
3. Ke krajním pixelům ve spojeném laplacianu zpět *přičtu* požadované okrajové hodnoty dle vstupní konfigurace (výchozí nastavení je přičtení hodnot z *base*, lze ale přičíst hodnoty z druhého obrázku, algoritmus sleduje kde došlo k překryvu okrajů a pouze na hranici, kde dochází k překryvu, přičítá hodnoty).

Výsledky jednotlivých konfigurací jsou lépe vidět dále v ukázkách.

## Ukázky

Mezi první pokusy patřila (poměrně neoriginálně) replikace ukázky z [wikipedie](https://en.wikipedia.org/wiki/Gradient-domain_image_processing).

<img src="hand.jpg" width="150">
<img src="eye.jpg" width="150">

Levý snímek je kontrolní výstup po 100 iteracích, vpravo finální výsledek:

    merge 950 550 

<img src="100it.png" width="150">
<img src="out.png" width="150">

U tohoto snímku jsem nemusel řešit okrajové podmínky, neboť přidaný snímek nesahal až do kraje.

### Bonsaj

Záhy jsem se přesnul k vlastní tvorbě. Jedním z mnoha dalších pokusů byla bonsaj, vstupem byly snímky:

<img src="bonsai_left.jpeg" width="200">
<img src="bonsai_right.jpeg" width="200">
<img src="bonsai_combined.jpeg" width="200">
<img src="bonsai_crop.jpeg" width="106.5">

kde jsem poměrně rychle zjistil, že u fotografií s ostrými přechody budou problémy s nenavazujícími hranami. Hraniční podmínky jsou 

- u levého snímku nastaveny na hodnoty v levém obrázku (`merge 470 -2 BBBB 5000`)
- na pravém snímku na hodnoty pravého obrázku. (`merge 470 -2 AAAA 5000`)

<img src="bonsai_border_left.png" width="400">
<img src="bonsai_border_right.png" width="400">

Počet iterací byl v obou případech omezen na 5000.

### Pokoj

Poměrně obdobný byl postup při tvrobě úvodního snímku, obrázky mají podobnou expozici:

<img src="room_left.jpeg" width="300">
<img src="room_right.jpeg" width="300">
<img src="bon_right.jpeg" width="145">

Výstup má na okrajích pracuje s hodnotami levého obrázku:

    merge 515 0 0000 10000

<img src="bon.png" width="400">

U tohoto snímku jsem ale zaznamenal, že pokud jsem nechal JM běžet okolo 50000 iterací, pak se v závislosti na posunu přidávaného obrázku ve výstupu začaly objevovat nežádoucí intenzity (mimo interval 0-255), které byly ořezány při ukládání:

<img src="bon1.png" width="400">
<img src="bon4.png" width="400">

Vyřešil jsem to omezením počtu iterací na cca 10000

### Stůl
Vstupem byly obrázky se znatelně jinou expozicí:

<img src="desk_left.jpeg" width="300">
<img src="cleft.jpeg" width="200">
<img src="desk_right.jpeg" width="300">

Zde byl znatelný vliv nastavení okrajů ve výstupu, nastavení je patrné z výstupů, pro jistotu:

- první snímek má okraje nastavené na intenzity pravé části snímku (`merge 0 0 BBBB 50000`)
- druhý snímek má okraje nastavené na intenzity levé části snímku (`merge 0 0 AAAA 50000`)
- a patrně nejlepší konfigurace - třetí snímek má okraje nastavené nahoře a dole na intenzity pravé části a vlevo na intenzity levé části (`merge 0 0 B0BA 50000`)

<img src="desk_all_right.png" width="400">
<img src="desk_all_left.png" width="400">
<img src="desk_left_left.png" width="400">

Ostrý přechod v na druhém snímku by bylo možné odstanit tím, že bych měl k dispozici intenzity celého tmavšího snímku. Nešťastně jsem se ale na začátku rozhodl pracovat pouze s ořezanou částí jednoho ze dvou vstupních snímků, musel bych tedy kód lehce doplnit, nešlo by ale o nic jiného, že o vytváření výřezu přímo v kódu.

### Jiné, méně povedené

Během práce jsem vyplodil i několik méně povedených ukázek, v zásadě ale vždy šlo o problémy s návazností snímků:

Vstup:

<img src="velMid.jpeg" width="300">
<img src="velSecond.jpeg" width="300">
<img src="velF.jpeg" width="200">

V následujících snímcích byl použit pouze jiný výřez z druhého snímku výše, výstupy:

<img src="vel.png" width="400">
<img src="vel1.png" width="400">
<img src="vel2.png" width="400">
<img src="vel3.png" width="400">


## Gauss-Seidel, Jacobiova metoda, CPU a GPU
Jen pro zajímavost, zde je porovnání výstupů z CPU, použití GS a GPU použití JM, výstup příkladu *stůl* po 1000 iteracích.

- CPU: snímek vlevo, metoda Gauss-Seidel, čas 31.863s, manuálně zastaveno na 1000 iteracích
- GPU: snímek vpravo, Jacobiova metoda, čas 1.167s, manuálně zastaveno na 1000 iteracích

<img src="deskCPU1000.png" width="400">
<img src="deskGPU1000.png" width="400">

Když jsem oba programy nechal běžet bez manuálního zastavení, dostal jsem prakticky totožný výsledek, pouze za jiný čas:

- CPU: snímek vlevo, metoda Gauss-Seidel, čas 20m 56.891s, program doběhl po cca 40000 iteracích
- GPU: snímek vpravo, Jacobiova metoda, čas 54.143s, program doběhl po cca 45000 iteracích

<img src="deskCPU50000.png" width="400">
<img src="deskGPU50000.png" width="400">


Pro zajímavost, graf maximálních změn mezi iteracemi (testováno po 1000 iteracích), vpravo totéž, pouze y má log. škálování. Průběh je pro všechny metody velmi podobný, GS potřeboval o něco méně iterací než JM.

<img src="edit.png" width="400">
<img src="editlog.png" width="400">

Co se OpenGL týče, tak používám GLEW a GLFW, pracuji na Linuxu, takže s podporou není problém, kdyby někdo chtěl někdy projekt kompilovat na Windows, tak asi bude potřeba něco přinastavit, na Linuxu postačí instalace těchto dvou knihoven a přiložený makefile se postará.

