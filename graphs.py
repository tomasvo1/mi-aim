import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import make_interp_spline, BSpline
from scipy.ndimage.filters import gaussian_filter1d

counts = np.array([8, 10, 12, 14, 15, 16, 17, 18, 19, 20])
counts = np.power([2], counts)
boxsizesSMOL = [200, 200, 200, 400, 400, 400, 400, 400]
boxsizesBIG = [1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 2000, 4000]

cpuLin0 = np.array([1, 7.2, 34.2, 500])
cpuWin0 = np.array([14.9, 15, 60.8, 838])
cpuLin1 = np.array([1, 7, 14, 50, 140, 450])
cpuWin1 = np.array([15, 15, 33, 140, 390, 880])
gpuLin2 = np.array([16, 16, 16, 16, 49, 183])
gpuWin2 = np.array([15, 15, 26.6, 361, 1412])
gpuLin3 = np.array([16, 16, 16, 16, 16, 16, 49, 133])
gpuWin3 = np.array([15, 15, 15, 25.5, 64, 203, 521])

Lin0b = [1, 7.5, 28.2, 481.5]
Lin1b = [1, 2, 7.5, 26.7, 180]
Lin2b = [16, 16, 16, 16, 182]
Lin3b = [16, 16, 16, 16, 16, 16, 33, 49, 66, 100]
Win0b = [15, 15, 51, 793]
Win1b = [15, 15, 15, 48, 349]
Win2b = [15, 15, 26.5, 354]
Win3b = [15, 15, 15, 17.7, 43, 106, 271, 680, 1020, 1730]

fig = plt.figure()
ax = fig.add_subplot(1, 1, 1)

for axl in ['top', 'bottom', 'left', 'right']:
    ax.spines[axl].set_linewidth(2)

plots = []


def locPlot(x, y, label):
    print(len(y))
    x = np.array(x[:len(y)])
    y = np.array(y)
    xnew = np.linspace(x.min(), x.max(), 300)
    spl = make_interp_spline(x, y, k=1)  #BSpline object
    ysmooth = spl(xnew)
    #ysmooth = gaussian_filter1d(y, sigma=1)
    p, = plt.plot(xnew, ysmooth, label=label)
    plots.append(p)



#locPlot(counts[:4], cpuLin0, "CPU naive GTX 1060")
#locPlot(counts[:6], cpuLin1, "CPU octree GTX 1060")
#locPlot(counts[:6], gpuLin2, "GPU naive GTX 1060")
#locPlot(counts[:8], gpuLin3, "GPU grid GTX 1060")
#locPlot(counts[:4], cpuWin0, "CPU naive GT 640M")
#locPlot(counts[:6], cpuWin1, "CPU octree GT 640M")
#locPlot(counts[:5], gpuWin2, "GPU naive GT 640M")
#locPlot(counts[:7], gpuWin3, "GPU grid GT 640M")

#top = 1400
#plt.axvline(pow(2, 14), color='black', linewidth='0.5')
#ax.text(pow(2, 14) - 5000, top, r'box $200 \times 200 \times 200$', horizontalalignment='right', fontsize=7)
#ax.text(pow(2, 14) + 20000, top, r'box $400 \times 400 \times 400$', horizontalalignment='left', rotation_mode='anchor', fontsize=7)

top = 1750
plt.axvline(pow(2, 18), color='black', linewidth='0.5')
plt.axvline(pow(2, 19), color='black', linewidth='0.5')
ax.text(pow(2, 18) - 100000, top, r'box $1000 \times 1000 \times 1000$', horizontalalignment='right', fontsize=7)
ax.text(pow(2, 18) + 20000, top, r'box $2000 \times 2000 \times 2000$', horizontalalignment='left', rotation_mode='anchor', fontsize=5, rotation=-90)
ax.text(pow(2, 19) + 40000, top, r'box $4000 \times 4000 \times 4000$', horizontalalignment='left', rotation_mode='anchor', fontsize=5, rotation=-90)

#locPlot(counts, Lin0b, "CPU naive GTX 1060")
#locPlot(counts, Lin1b, "CPU octree GTX 1060")
#locPlot(counts, Lin2b, "GPU naive GTX 1060")
#locPlot(counts, Lin3b, "GPU grid GTX 1060")
locPlot(counts, Win0b, "CPU naive GT 640M")
locPlot(counts, Win1b, "CPU octree GT 640M")
locPlot(counts, Win2b, "GPU naive GT 640M")
locPlot(counts, Win3b, "GPU grid GT 640M")

plt.legend(handles=plots, loc='upper left', prop={'size': 7})
plt.grid(True, which="both", linestyle="dotted")
plt.xlabel('number of boids')
plt.ylabel('time per frame [ms]')
plt.xscale("log")
#plt.yscale("log")

for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
             ax.get_xticklabels() + ax.get_yticklabels()):
    item.set_fontsize(7)

#plt.show()
plt.savefig('winB.svg')
