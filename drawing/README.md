# Segmentace obrazu

Úlohu jsem řešil pomocí knihovny GridCut, implementace je v souboru [drawing.cpp](../src/drawing.cpp) ke každému testovacímu snímku jsem si vytvořil scribble. Původně jsem plánoval pomocí GLFW/OpneGL vyrobit interaktivní klikátko, ale nezbyl čas, možná dodatečně se něco objeví. 

Chvíli jsem zápolil s "vylívéním" barev do pozadí, to jsem nakonec vyřešil automatickým přidáním rámečku, který má buďto barvu která do rámečku zasahuje, nebo je bezbarvý (interně -1).

Na vstupu se krom dvojice obrázů bere dvojice hodnot - `gamma` (pro gamma korekci) a `lambda` (pro úpravu kapacit `K` napojených na zdroj a stok, které se počítají jako max součtu sousedních hran).

Během implementace posloužila kytka narychle nakreslená v GIMPu:


    open drawDrawing.png
    open drawScribble.png
    fill 1 0.05 # gamma = 1, lambda = 0.05
    save png draw

<img src="drawDrawing.png" width="200">
<img src="drawScribble.png" width="200">
<img src="drawMult.png" width="200">
<img src="draw.png" width="200">

## Další příklady

Běžně se nastavení `lambda` pohybovalo okolo 0.05 (tedy dle očekávání), někdy jsem trochu lepší výsledky dostal s nižším koeficientem, `gamma` se pohybovala běženě mezi 1 a 3.

### Kostky
<img src="geometryDrawing.jpg" width="200">
<img src="geometryScribble.png" width="200">
<img src="geometryMult.jpg" width="200">
<img src="geometry.png" width="200">

### Lístky
<img src="plantsDrawing.jpg" width="200">
<img src="plantsScribble.png" width="200">
<img src="plantsMult.jpg" width="200">
<img src="plantsA.png" width="200">


<img src="plantsDrawing.jpg" width="200">
<img src="plantsScribbleB.png" width="200">
<img src="plantsMultB.jpg" width="200">
<img src="plantsB.png" width="200">

### Sklenka

Tady jsem zápolil se slíváním barev, malé tvary dělaly neplechu a použitý scribble se zahušťoval, až jsem ho nakonec nechal být, s tím, že drobné lístky by bylo už jednodušší vybarvit ručně...

<img src="glassDrawing.jpg" width="200">
<img src="glassScribble.png" width="200">
<img src="glassMult.jpg" width="200">
<img src="glass.png" width="200">

### Budova

<img src="architectureDrawing.jpg" width="200">
<img src="architectureScribble.png" width="200">
<img src="architectureMult.png" width="200">
<img src="architecture.png" width="200">

### Ještě jedny kytičky
V tomto obrázku byly taky nějaké drobné lístky, ale tentokrát (lepší scribble?) se zdá že se barvení povedlo lépe...?

<img src="flowersDrawing.jpg" width="200">
<img src="flowersScribble.png" width="200">
<img src="flowersMult.png" width="200">
<img src="flowers.png" width="200">

### Kávovar
Nakonec vlastní kresba ze druhého ročníku (asi ?) bakaláře na FITu. 

<img src="coffeeDrawing.jpg" width="200">
<img src="coffeeScribbleA.png" width="200">
<img src="coffeeMultA.jpg" width="200">
<img src="coffeeB.png" width="200">

    open drawing/coffeeDrawing.jpg
    open drawing/coffeeScribbleC.png
    fill 3 0.01 # gamma = 1, lambda = 0.01
    save png drawN

<img src="coffeeDrawing.jpg" width="200">
<img src="coffeeScribbleC.png" width="200">
<img src="coffeeMultB.jpg" width="200">
<img src="coffeeA.png" width="200">