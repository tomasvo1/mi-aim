# Fourierova transformace
Implementace je provedena v C++ pomocí knihovny FFTW, konečný shift kvadrantů jsem dodělával sám (chvilku to trvalo). Níže je pár obrázků jak umělých pro ilustraticní účely, tak fotografií. Na každý "snímek" amplitudového spektra jsem jsem aplikoval logaritmické přeškálování s koeficientem okolo 1000, aby bylo něco vidět. Bez něj stejnosměrná složka uprostřed obrazu všechno přehluší.

    open pulse.png
    loaded 1 channels
    fft
    logscale 1000
    save png fpulse
    open tpulse.png
    loaded 1 channels
    fft
    logscale 1000
    save png ftpulse

<img src="pulse.png" width="200">
<img src="fpulse.png" width="200">

<img src="tpulse.png" width="200">
<img src="ftpulse.png" width="200">

    open stripes.png
    loaded 1 channels
    fft
    logscale 1000
    save png fstripes
    open stripes2.png
    loaded 1 channels
    fft
    logscale 1000
    save png fstripes2

<img src="stripes.png" width="200">
<img src="fstripes.png" width="200">
<img src="stripes2.png" width="200">
<img src="fstripes2.png" width="200">

    open cicasqr.png
    fft
    logscale 10000
    save png fcica

<img src="cicasqr.png" width="200">
<img src="fcica.png" width="200">


    open lena.png
    loaded 1 channels
    fft
    logscale 10000

<img src="lena.png" width="200">
<img src="flena.png" width="200">

    open duck.png
    loaded 1 channels
    fft
    logscale 10000
    save png fduck

<img src="duck.png" width="200">
<img src="fduck.png" width="200">
