#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb/stb_image.h"
#include "stb/stb_image_write.h"
#include "include/editor.hpp"
#include "include/gl.hpp"
#include <iostream>


//MANAGEMENT STUFF
Editor::Editor()
: image_stack_filled(0)
{
    for (size_t i = 0; i < STACKSIZE; i++)
       images[i].imagef = nullptr;
    
    histoimage = new unsigned char[HISTOWIDTH * HISTOHEIGHT];

    initGL();
}

Editor::~Editor()
{
    for (size_t i = 0; i < STACKSIZE; i++)
        if (images[i].imagef != nullptr)
        {
            delete [] images[i].imagef;
            stbi_image_free(images[i].image);
        }

    delete [] histoimage;
    finalizeGL();
}

void Image::deinterlieve()
{
    //cout << "deinterlieving " << channels << "channels" << endl;
    unsigned char * tmp = new unsigned char[size];
    
    for (size_t c = 0; c < channels; c++)
        for (size_t i = 0; i < width * height; i++)
            tmp[c * size_plane + i] = image[i * channels + c];
    
    for (size_t i = 0; i < size; i++)
        image[i] = tmp[i];

    tofloats();

    for (size_t c = 0; c < channels; c++)
    {
        //cout << "init plane" << c << endl;
        planes[c].image = &(image[size_plane * c]);
        planes[c].imagef = &(imagef[size_plane * c]);
    }
    
    delete [] tmp;
}

void Image::interlieve()
{
    unsigned char * tmp = new unsigned char[size];
    for (size_t c = 0; c < channels; c++)
        for (size_t i = 0; i < width * height; i++)
            tmp[i * channels + c] = image[c * size_plane + i];
    
    for (size_t i = 0; i < size; i++)
        image[i] = tmp[i];

    delete [] tmp;
}

void Image::tofloats(){
    if (!imagef)
        imagef = new float[size];
    
    for (size_t i = 0; i < size; i++)
        imagef[i] = image[i] / 255.0f;
}

void Image::pixel_lab(size_t idx, float out[3])
{
    //in case grayscale, it is simple
    if (channels == 1)
    {
        out[0] = imagef[idx];
        out[1] = 0;
        out[2] = 0;
        return;
    }

    //in other case we have three components
	float r = planes[0].imagef[idx],  g = planes[1].imagef[idx], b = planes[2].imagef[idx];

	if (r > 0.04045f)
		r = powf(((r + 0.055f) / 1.055f), 2.4f);
	else 
		r = r / 12.92f;

	if (g > 0.04045)
		g = powf(( (g + 0.055f) / 1.055f), 2.4f);
	else
		g = g / 12.92f;

	if (b > 0.04045f)
		b = powf(( (b + 0.055f) / 1.055f), 2.4f);
	else
		b = b / 12.92f;

	r = r * 100;
	g = b * 100;
	b = b * 100;

	//Observer. = 2°, Illuminant = D65, divided by ref values
	float x = (r * 0.4124f + g * 0.3576f + b * 0.1805f) / 95.047f;
	float y = (r * 0.2126f + g * 0.7152f + b * 0.0722f) / 100.0f;
	float z = (r * 0.0193f + g * 0.1192f + b * 0.9505f) / 108.883f;

	if ( x > 0.008856 ) 
		x = powf(x , ( 1.0f/3 )); 
	else 
		x = ( 7.787 * x ) + ( 16.0f/116 );

	if ( y > 0.008856 )
		y = powf(y , ( 1.0f/3 )); 
	else
	    y = ( 7.787 * y ) + ( 16.0f/116 );

	if ( z > 0.008856 )
		z = powf(z , ( 1.0f/3 )); 
	else 
		z = ( 7.787 * z ) + ( 16.0f/116 );

	out[0] = ( 116 * y ) - 16;
	out[1] = 500 * ( x - y );
	out[2] = 200 * ( y - z );
}

 void Image::pixel_rgb(size_t idx, float out[3])
 {
    //in case grayscale, it is simple
    if (channels == 1)
    {
        out[0] = imagef[idx];
        out[1] = 0;
        out[2] = 0;
        return;
    }

    out[0] = planes[0].imagef[idx];
    out[1] = planes[1].imagef[idx];
    out[2] = planes[2].imagef[idx];
 }

int Editor::load_image(const string & filename)
{
    if (image_stack_filled == STACKSIZE - 1)
        return NOIMAGESPACE;

    shake_stack_up();
    Image & image = images[0];

    image.filename = filename;
    image.image = stbi_load(filename.c_str(), 
                            &image.width, &image.height, 
                            &image.channels, 0); 

    if (image.image == nullptr)
        return NOFILE;

    image.size_plane = image.width * image.height;
    image.size = image.size_plane * image.channels;
    image.deinterlieve();

    cout << "loaded " << image.channels << " channels" << endl;            
    return OPOK;
}

//empty image with dimensions based on the top image
Image Editor::empty_image()
{
    if (images[0].imagef == nullptr)
        return Image();

    Image image;
    image.filename = images[0].filename + "_copy";
    image.channels = images[0].channels;
    image.size_plane = images[0].size_plane;
    image.size = image.channels * image.size_plane;

    image.image = new unsigned char[image.size];
    image.imagef = new float[image.size];
  
    for (size_t i = 0; i < image.size; i++)
    {
        image.image[i] = 255;
        image.imagef[i] = 1.0f;
    }

    //memset(image.image, 0, sizeof(unsigned char) * images[0].size);
    //memset(image.imagef, 0, sizeof(float) * images[0].size);
    
    for (size_t i = 0; i < image.channels; i++)
    {
        image.planes[i].image = &(image.image[image.size_plane * i]);
        image.planes[i].imagef = &(image.imagef[image.size_plane * i]);
    }
    
    return image;
}

void Editor::discard_image(Image & image)
{
    delete [] image.image;
    delete [] image.imagef;
}

int Editor::drop_image(size_t index){

    if (index >= STACKSIZE || images[index].imagef == nullptr)
        return EMPTYIMAGESLOT;

    delete [] images[index].imagef;
    images[index].imagef = nullptr;
    stbi_image_free(images[index].image);
    images[index].image = nullptr;
    images[index].filename.clear();

    shake_stack_down();
    return OPOK;
}

void Editor::shake_stack_down()
{
    //shake stack down towards lower indices
    for (size_t i = 0; i < STACKSIZE; ++i)
    {
        for (size_t j = 0; j < i; j++)
        {
            if (images[j].imagef == nullptr)
            {
                images[j] = images[i];
                images[i].imagef = nullptr;
                images[i].image = nullptr;
            }
        }
    }
}

void Editor::shake_stack_up()
{
    if (image_stack_filled == STACKSIZE)
        return;

    //shake stack up by one index
    for (size_t i = STACKSIZE - 1; i > 0; --i)
        images[i] = images[i - 1];

    images[0].imagef = nullptr;
    images[0].image = nullptr;

    image_stack_filled++;
}

int Editor::save_image(const string & format, const string & filename)
{
    if (images[0].imagef == nullptr)
        return EMPTYIMAGESLOT;

    Image & image = images[0];

    discretize(image);
    image.interlieve();

    if (format == "jpg")
    {
        stbi_write_jpg((filename + ".jpg").c_str(), image.width, image.height, image.channels, image.image, 0);
        return OPOK;
    } else if (format == "png")
    {
        stbi_write_png((filename + ".png").c_str(), image.width, image.height, image.channels, image.image, 0);
        return OPOK;
    } else 
        return UNKNOWNFORMAT;
}

void Editor::discretize(Image & image)
{
    for (size_t i = 0; i < image.size; i++)
        image.image[i] = max(min(image.imagef[i], 1.0f), 0.0f) * 255 + 0.5;
}

const Image * Editor::get_image(size_t index) const
{
    if (index >= STACKSIZE || images[index].imagef == nullptr)
        return nullptr;

    return images + index;
}

int Editor::histogram(const string & filename)
{
    Image & image = images[0];

    if(image.image == nullptr)
        return EMPTYIMAGESLOT;

    size_t histo[256];
    for (size_t i = 0; i < 256; i++)
        histo[i] = 0;

    discretize(image);

    //accumulate
    for (size_t i = 0; i < image.size; i++)
        histo[image.image[i]]++;

    size_t maximum = 0;
    for (size_t i = 0; i < 256; i++)
        maximum = max(maximum, histo[i]);

    
    float step;
    if (!filename.empty())
    {      
        //DRAW HISTOGRAM INTO FILE
        step = (float) maximum / HISTOHEIGHT;
        for (size_t i = 0; i < 256; i++)
        {
            for (size_t k = 0; k < HISTOHEIGHT; k++)
            {
                if (histo[i] < maximum - k * step)
                {
                    for (size_t j = 0; j < BARWIDTH; j++)
                        histoimage[histindex(i * BARWIDTH + j, k)] = 255;
                } else {
                    for (size_t j = 0; j < BARWIDTH; j++)
                        histoimage[histindex(i * BARWIDTH + j, k)] = 0;
                }
            }
        }

        //save image
        stbi_write_png((filename + ".png").c_str(), HISTOWIDTH, HISTOHEIGHT, 1, histoimage, 0);
    } else {

        //DRAW HISTOGRAM INTO TERMINAL
        step = (float) maximum / (HISTOHEIGHTTERM * 2);
        
        //compress histogram for terminal
        size_t locmax;
        for (size_t i = 0; i < 256 / HISTOBINTERM; i++)
        {
            locmax = 0;
            for (size_t j = 0; j < HISTOBINTERM; j++)
                locmax = max(locmax, histo[i * HISTOBINTERM + j]);
            histo[i] = locmax;
        }

        //terminal output
        string histoterm;

        for (size_t k = 0; k < HISTOHEIGHTTERM; k++)
        {
            for (size_t i = 0; i < 256 / HISTOBINTERM; i++)
            {
                if (histo[i] < maximum - (k * 2 + 1) * step)
                    histoterm.push_back(' ');
                else if (histo[i] < maximum - (k * 2) * step)
                    histoterm.push_back('-');
                else
                    histoterm.push_back('#');      
            }

            histoterm.push_back('\n');
        }
        cout << histoterm << endl;
    }

    return OPOK;
}