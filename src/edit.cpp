#include <iostream>
#include <cstring>
#include <cmath>
#include "include/editor.hpp"
#include "include/gl.hpp"

using namespace std;

/*void laplacian(Image & im, size_t plane_idx, float * lapl, unsigned char include_boundry)
{
    size_t pos = 0;
    float * img = im.planes[plane_idx].imagef;

    for (size_t i = 0; i < im.height; i++)
        for(size_t j = 0; j < im.width; j++)
        {
            //adding each neighboring cell
            //setting also boundry conditions
            lapl[pos] = 
                (4 * img[pos] 
                    - (i == 0 ? (include_boundry * img[pos]) : img[pos - im.width]) 
                    - (j == 0 ? (include_boundry * img[pos]) : img[pos - 1])
                    - (i == (im.height - 1) ? (include_boundry * img[pos]) : img[pos + im.width])
                    - (j == (im.width - 1) ? (include_boundry * img[pos]) : img[pos + 1])
                );
            pos++;          
        }        
}*/

void laplacian(Image & im, size_t plane_idx, float * lapl)
{
    size_t pos = 0;
    float * img = im.planes[plane_idx].imagef;

    for (size_t i = 0; i < im.height; i++)
        for(size_t j = 0; j < im.width; j++)
        {
            //adding each neighboring cell
            //boundary conditions are setup later!!
            lapl[pos] = 
                (4 * img[pos] 
                    - (i == 0 ? img[pos] : img[pos - im.width]) 
                    - (j == 0 ? img[pos] : img[pos - 1])
                    - (i == (im.height - 1) ? img[pos] : img[pos + im.width])
                    - (j == (im.width - 1) ? img[pos] : img[pos + 1])
                );
            pos++;          
        }        
}

int Editor::edit(int x, int y, unsigned int boundry, unsigned int maxiter){

    Image & top = images[0]; 
    Image & bottom = images[1];

    if(top.imagef == nullptr || bottom.imagef == nullptr)
        return EMPTYIMAGESLOT;

    if (top.channels != bottom.channels)
        return UNSUPPORTED;

    float * top_lap = new float[top.size_plane];
    float * bottom_lap = new float[bottom.size_plane];
    float * merged = new float[bottom.size_plane];
    float * copy = new float[bottom.size_plane];

    EditProgram gpu;

    //each channel seperately
    for (size_t c = 0; c < top.channels; c++)
    {
        //compute divergence of both of the pictures
        //boundary conditions are not set!!
        laplacian(top, c, top_lap);
        laplacian(bottom, c, bottom_lap);
        memcpy(merged, bottom.planes[c].imagef, bottom.size_plane * sizeof(float));

        size_t idx;
        //paste top laplacian into the bottom
        for (size_t i = 0; i < top.height; i++)
        {
            for (size_t j = 0; j < top.width; j++)
            {   
                
                //test if we are inside the bottom image
                if (y + i >= bottom.height || x + j >= bottom.width ||
                    y + i < 0 || x + j < 0)
                    continue;

                idx = (y + i) * bottom.width + x + j;
                bottom_lap[idx] = top_lap[i * top.width + j];
                //bottom.planes[c].imagef[idx] = 0;
                merged[idx] = top.planes[c].imagef[i * top.width + j];
            }
        }

        //SETUP BOUNDARY CONDITIONS
        //
        //if the boundary is set to NONE or BASE, 
        //the bottom (aka base) image value is added (previously subtracted in the laplace function), 
        //as if the inensity of the original border pixels were 0
        //
        //if the boundary is set to ADDED
        //and the border pixel indeed lies in the added image (top),
        //then the intensity of the border pixel is added as if the nighboring value is 0 (aka border)

        bool overalp_bottom = (y + top.height >= bottom.height);
        bool overlap_top = (y <= 0);
        
        int ti, bi = (bottom.height - 1) * bottom.width; 
        for (int t = 0; t < bottom.width; t++)
        {
            ti = t;

            if (MergeMask::TopNone & boundry || MergeMask::TopBase & boundry)
                bottom_lap[ti] += bottom.planes[c].imagef[ti];
            else if ((MergeMask::TopAdded & boundry) && overlap_top)
            {
                //test if we are inside overlap
                if (t >= x && t < x + top.width)
                    bottom_lap[ti] += top.planes[c].imagef[(-y) * top.width + ti - x]; // copy boundry values of top image
                else
                    bottom_lap[ti] += bottom.planes[c].imagef[ti]; // copy boundary values of bottom image
            } else {
                bottom_lap[ti] += bottom.planes[c].imagef[ti]; // default
            }

            if (MergeMask::BottomNone & boundry || MergeMask::BottomNone & boundry)
                bottom_lap[bi] += bottom.planes[c].imagef[bi];
            else if ((MergeMask::BottomAdded & boundry) && overalp_bottom)
            {
                //test if we are inside overlap
                if ((t >= x && t < x + top.width))
                    bottom_lap[bi] += top.planes[c].imagef[(bottom.height - y - 1) * top.width + ti - x]; // copy boundry values of top image
                else
                    bottom_lap[bi] += bottom.planes[c].imagef[bi]; // copy boundary values of bottom image
            } else {
                bottom_lap[bi] += bottom.planes[c].imagef[bi]; // default
            }
        
            bi++;
        }

        //same here for left and right
        bool overalp_right = (x + top.width >= bottom.width);
        bool overlap_left = (x <= 0);

        int li, ri;
        for (int l = 0; l < bottom.height; l++)
        {
            li = l * bottom.width;
            ri = li + bottom.width - 1;
            
            if (MergeMask::LeftNone & boundry || MergeMask::LeftBase & boundry)
                bottom_lap[li] += bottom.planes[c].imagef[li];
            else if ((MergeMask::LeftAdded & boundry) && overlap_left)
            {
                //test if inside overlap
                if (l >= y && l < y + top.height)
                    bottom_lap[li] += top.planes[c].imagef[((l - y) * top.width + (-x))]; // copy boundry values of top image
                else 
                    bottom_lap[li] += bottom.planes[c].imagef[li]; // copy boundary values of bottom image
            } else {
                bottom_lap[li] += bottom.planes[c].imagef[li]; //default
            }

            if (MergeMask::RightNone & boundry || MergeMask::RightBase & boundry)
                bottom_lap[ri] += bottom.planes[c].imagef[ri];
            else if ((MergeMask::RightAdded & boundry) && overalp_right)
            {
                //test if inside overlap
                if (l >= y && l < y + top.height)
                {
                    bottom_lap[ri] += top.planes[c].imagef[((l - y) * top.width + x)]; // copy boundry values of top image
                }
                else 
                    bottom_lap[ri] += bottom.planes[c].imagef[ri]; // copy boundary values of bottom image
            } else {
                bottom_lap[ri] += bottom.planes[c].imagef[ri]; //default
            }

        }   
        
        cout << "processing channel " << c << endl;
#define GPU

#ifndef GPU
        //CPU VERSION Gauss-Seidel
        float * img = merged;
        float difff = 0;
        for (size_t it = 0; it < maxiter; it++)
        {
            size_t pos = 0;
            for (size_t i = 0; i < bottom.height; i++)
            {
                for (size_t j = 0; j < bottom.width; j++)
                {   
                    //img[pos] = img[pos];
                    img[pos] = (i == 0) ? 0 : img[pos - bottom.width];
                    img[pos] += (j == 0) ? 0 : img[pos - 1];
                    img[pos] += (i == (bottom.height - 1)) ? 0 : img[pos + bottom.width];
                    img[pos] += (j == (bottom.width - 1)) ? 0 : img[pos + 1];
                    img[pos] += bottom_lap[pos];
                    img[pos] *= 0.25;
                    pos++;  
                }
            }

            if (it % FOLD == 0)
            {
                
                float diff = 0;
                for (size_t i = 0; i < bottom.size_plane; i++)
                {
                    diff = max(diff, fabs(img[i] - copy[i]));
                    copy[i] = img[i];
                }
                
                difff = diff * 255;
                cout << difff << " " << it << "\n" << flush;
            } 

            if (difff < THRESHOLD)
                break;
        }
#else
        //GPU using Jacobi method
        gpu.process(merged, bottom_lap, bottom.width, bottom.height, maxiter);
#endif
    memcpy(bottom.planes[c].imagef, merged, bottom.size_plane * sizeof(float));
    }

    drop_image(0);
    delete [] top_lap;
    delete [] bottom_lap;
    delete [] merged;
    delete [] copy;

    return OPOK;
}