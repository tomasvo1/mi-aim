#include <set>
#include <iostream>
#include <cmath>
#include "include/editor.hpp"
#include "gridcut/include/GridCut/GridGraph_2D_4C.h"

using namespace std;

//template params: cap nodes-terminals, nodes-neighbors, flow
typedef GridGraph_2D_4C<short, short, int> Grid;

void get_colors(set<Color> &colors, Image &scribble)
{
    Color c;
    for (size_t p = 0; p < scribble.size_plane; p++)
    {
        c.r = scribble.planes[0].imagef[p];
        c.g = scribble.planes[1].imagef[p];
        c.b = scribble.planes[2].imagef[p];

        if (colors.find(c) == colors.end())
            colors.insert(c);
    }
}

//expects black-white image, so that only the first plane can be used
//to compute the weights
float big_K(Image &im, float gamma)
{
    float K = 0, localK;
    size_t pos = 0;

    float *img = im.planes[0].imagef;
    for (size_t i = 0; i < im.height; i++)
        for (size_t j = 0; j < im.width; j++)
        {
            //adding each neighboring pixel
            localK = 1;
            localK += (i == 0 ? 0 : (1 + min(254.f, 255 * pow(min(img[pos], img[pos - im.width]), gamma))));
            localK += (j == 0 ? 0 : (1 + min(254.f, 255 * pow(min(img[pos], img[pos - 1]), gamma)))); 
            localK += (i == (im.height - 1) ? 0 : (1 + min(254.f, 255 * pow(min(img[pos], img[pos + im.width]), gamma)))); 
            localK += (j == (im.width - 1) ? 0 : (1 + min(254.f, 255 * pow(min(img[pos], img[pos + 1]), gamma))));
            K = max(K, localK);
            pos++;
        }

    return K;
}

bool white(const Color &c)
{
    return (c.r == 1 && c.g == 1 && c.b == 1);
}

Color mask_background(Image & scribble)
{
    Color back = Color{-1, -1, -1};
    Color pixel;

    size_t padding = max((int) (min(scribble.width, scribble.height) * 0.05), 5);

    for (size_t i = 0; i < scribble.width; i++)
        for (size_t j = 0; j < scribble.height; j++)
        {
            scribble.pixel_rgb(j * scribble.width + i, pixel.pixel);
            
            if (((i < padding) || (i > scribble.width - padding) ||
                 (j < padding) || (j > scribble.height - padding)) && !white(pixel))
            {
                back = pixel;
            }
        }



    size_t bottom = (scribble.height - padding) * scribble.width;
    for (size_t i = 0; i < scribble.width; i++)
    {
        for (size_t j = 0; j < padding; j++)
        {
            scribble.planes[0].imagef[j * scribble.width + i] = back.r; 
            scribble.planes[1].imagef[j * scribble.width + i] = back.g; 
            scribble.planes[2].imagef[j * scribble.width + i] = back.b; 

            scribble.planes[0].imagef[bottom + j * scribble.width + i] = back.r; 
            scribble.planes[1].imagef[bottom + j * scribble.width + i] = back.g; 
            scribble.planes[2].imagef[bottom + j * scribble.width + i] = back.b; 
        }
    }
    
    size_t right = scribble.width - padding;
    for (size_t i = 0; i < scribble.height; i++)
    {
        for (size_t j = 0; j < padding; j++)
        {
            scribble.planes[0].imagef[i * scribble.width + j] = back.r; 
            scribble.planes[1].imagef[i * scribble.width + j] = back.g; 
            scribble.planes[2].imagef[i * scribble.width + j] = back.b; 

            scribble.planes[0].imagef[i * scribble.width + j + right] = back.r; 
            scribble.planes[1].imagef[i * scribble.width + j + right] = back.g; 
            scribble.planes[2].imagef[i * scribble.width + j + right] = back.b; 
        }
    }
    

    return back;
}

bool default_background(const Color &c)
{
    return (c.r == -1 && c.g == -1 && c.b == -1);
}


void color_image(const Color & c, Image & drawing, 
                 Image & scribble, Image & colorful, 
                 float K, float gamma, 
                 Color & back_color, bool * processed)
{
    cout << c.r << " " << c.g << " " << c.b << endl;
    //use white as a default unmarked color
    if (white(c))
        return;

    Grid *grid = new Grid(drawing.width, drawing.height);
    Color pixel;
    size_t pos = 0;
    bool source;
    for (int y = 0; y < drawing.height; y++)
    {
        for (int x = 0; x < drawing.width; x++)
        {
            //set pixels and flags
            scribble.pixel_rgb(pos, pixel.pixel);
            source = (c == pixel);
            //set graph terminals
            grid->set_terminal_cap(grid->node_id(x, y),
                                   //set K if on the way to source
                                   source ? K : 0,
                                   //if not on the way to source and is not white
                                   (!source && !white(pixel)) ? K : 0);
            if (x < drawing.width - 1)
            {
                const short cap = 1 + min(pow(min(drawing.imagef[pos], drawing.imagef[pos + 1]), gamma) * 255, 254.f);
                grid->set_neighbor_cap(grid->node_id(x, y), +1, 0, cap);
                grid->set_neighbor_cap(grid->node_id(x + 1, y), -1, 0, cap);
            }
            if (y < drawing.height - 1)
            {
                const short cap = 1 + min(pow(min(drawing.imagef[pos], drawing.imagef[pos + drawing.width]), gamma) * 255, 254.f);
                grid->set_neighbor_cap(grid->node_id(x, y), 0, +1, cap);
                grid->set_neighbor_cap(grid->node_id(x, y + 1), 0, -1, cap);
            }
            pos++;
        }
    }

    //compute maxflow
    grid->compute_maxflow();
    pos = 0;

    for (int y = 0; y < drawing.height; y++)
    {
        for (int x = 0; x < drawing.width; x++)
        {
            if (!grid->get_segment(grid->node_id(x, y)) && !processed[pos])
            {
                //if in source component and not previously processed
                if (!default_background(c))
                {
                    colorful.planes[0].imagef[pos] = c.r;
                    colorful.planes[1].imagef[pos] = c.g;
                    colorful.planes[2].imagef[pos] = c.b;
                }
                processed[pos] = true;
            }
            pos++;
        }
    }
    delete grid;
}


int Editor::draw_segment(bool interactive, float gamma, float lambda)
{

    Image &scribble = images[0];
    Image &drawing = images[1];

    if (scribble.imagef == nullptr || drawing.imagef == nullptr)
    {
        cout << "missing image" << endl;
        return EMPTYIMAGESLOT;
    }

    if ((scribble.channels < drawing.channels) || (drawing.channels != 3))
    {
        cout << "channels do not match" << endl;
        return UNSUPPORTED;
    }

    if ((scribble.width != drawing.width) || (scribble.height != drawing.height))
    {
        cout << "dimensions do not match" << endl;
        return UNSUPPORTED;
    }

    //set of colors in the scribble
    set<Color> colors;
    get_colors(colors, scribble);

    //add image mask border to preserve background
    Color back_color = mask_background(scribble);

    //color mask
    bool *processed = new bool[scribble.size_plane];
    memset(processed, 0, sizeof(bool) * scribble.size_plane);
    Image colorful = empty_image();


    //calculate K and guess lambda
    float K = big_K(drawing, gamma) * lambda;
    cout << "K " << K << endl;

    //process background first if there is 
    if (!default_background(back_color))
    {
        color_image(back_color, drawing, scribble, colorful, K, gamma, back_color, processed);
    }
    
    //remove processed background if there is any
    colors.erase(back_color);
    
    //for each color in the scribble
    for (const auto &c : colors)
        color_image(c, drawing, scribble, colorful, K, gamma, back_color, processed);

    //multiply drawing and colorized version
    for (size_t i = 0; i < drawing.size; i++)
        drawing.imagef[i] *= colorful.imagef[i]; 
    
    //remove the color temporary image
    discard_image(colorful);
    
    //dropping scribbles!!
    drop_image(0);

    delete[] processed;
    return OPOK;
}