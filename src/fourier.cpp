#include <iostream>
#include <cmath>
#include "include/editor.hpp" 
#include <complex.h>  
#include <fftw3.h>  

using namespace std;


//possibly usable for future implementation of inverse dft
void fftshift(float * data, size_t w, size_t h, bool is_inverse)
{
    size_t top = h / 2;

    if (h % 2 == 1)
        top += is_inverse ? 0 : 1;

    size_t bottom = h - top;
    size_t left = w / 2;

    if (w % 2 == 1)
        left += is_inverse ? 0 : 1;

    size_t right = w - left;


    float * tmpdata = new float[w * h];

    cout << w << " " << h << " " << left << " " << top << " " << right << endl;

    //1 to 4
    size_t start4n = bottom * w + right;
    size_t start4o = top * w + right;
    for (size_t i = 0; i < top * left; i++)
        tmpdata[start4n + i + (i / left) * right] = data[i + (i / left) * right];
    
    //2 to 3
    size_t start3n = bottom * w;
    size_t start3o = top * w;
    size_t start2 = left;
    for (size_t i = 0; i < top * right; i++)
        tmpdata[start3n + i + (i / right) * left] = data[start2 + i + (i / right) * left];

    //3 to 2
    for (size_t i = 0; i < top * right; i++)
        tmpdata[start2 + i + (i / left) * right] = data[start3o + i + (i / left) * right];

    //4 to 1
    for (size_t i = 0; i < top * right; i++)
        tmpdata[i + (i / right) * left] = data[start4o + i + (i / right) * left];


    for (size_t i = 0; i < w * h; i++)
        data[i] = tmpdata[i];

    delete [] tmpdata;
}

int Editor::fft_amplitude()
{
    Image & image = images[0];

    if(image.image == nullptr)
        return EMPTYIMAGESLOT;

    if (image.channels != 1)
    {
        cout << "only implemented for grayscale images" << endl;
        return UNSUPPORTED;
    }

    fftw_complex * in;
    fftw_complex *out;
    fftw_plan my_plan;

    discretize(image);

    in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * image.size);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * image.size);
    
    for (size_t i = 0; i < image.size; i++)
    {
        in[i][0] = (double) image.imagef[i];
        in[i][1] = 0;
    }


    my_plan = fftw_plan_dft_2d(image.width, image.height, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    fftw_execute(my_plan);

    float maximum = 0;
    for (size_t i = 0; i < image.size; i++)
    {
        image.imagef[i] = sqrt(out[i][0] * out[i][0] + out[i][1] * out[i][1]);
        maximum = fmax(image.imagef[i], maximum);
    }

    for (size_t i = 0; i < image.size; i++)
        image.imagef[i] /= maximum;

    
    fftshift(image.imagef, image.width, image.height, false);
    //logscale(100);

    fftw_destroy_plan(my_plan);
    fftw_free(in);
    fftw_free(out);

    return OPOK;
}
