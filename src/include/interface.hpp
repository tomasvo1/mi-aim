#pragma once
#include "editor.hpp"
#include <unordered_map>

using namespace std;

class Interface;

typedef void (Interface::*imethod)(istringstream & iss);

class Interface {
public:
    Interface();
    void run();


protected:
    void load_image(istringstream & iss);
    void drop_image(istringstream & iss);
    void save_image(istringstream & iss);
    void print_help(istringstream & iss);
    void print_image_stack(istringstream & iss);
    void histogram(istringstream & iss);

    void negative(istringstream & iss);
    void threshold(istringstream & iss);
    void brightness(istringstream & iss);
    void contrast(istringstream & iss);
    void gamma(istringstream & iss);
    void nonlincontrast(istringstream & iss);
    void logscale(istringstream & iss);
    void quantize(istringstream & iss);
    void equalize(istringstream & iss);

    void fft(istringstream & iss);
    void gauss(istringstream & iss);
    void bilateral_filter(istringstream & iss);
    void merge(istringstream & iss);
    void scribble(istringstream & iss);

    Editor editor;
    std::unordered_map<std::string, imethod> calls;

};
