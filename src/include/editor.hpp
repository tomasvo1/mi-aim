#pragma once
#include <cstdlib>
#include <string>

//image stack size
#define STACKSIZE 16

//out codes
#define NOIMAGESPACE -1
#define EMPTYIMAGESLOT -2
#define UNKNOWNFORMAT -3
#define NOFILE -4
#define UNSUPPORTED -5
#define OPOK 0

//formats
#define PNG 0
#define JPG 1

//HISTOGRAM SETTINGS
#define BARWIDTH 3
#define HISTOWIDTH 256 * BARWIDTH
#define HISTOHEIGHT 500

#define HISTOBINTERM 4
#define HISTOHEIGHTTERM 20

#define histindex(x, y) ((y) * HISTOWIDTH + (x))

using namespace std;

enum MergeMask {
    TopNone = 1,
    TopBase = 2,
    TopAdded = 4,
    RightNone = 8,
    RightBase = 16,
    RightAdded = 32,
    BottomNone = 64,
    BottomBase = 128,
    BottomAdded = 256,
    LeftNone = 512,
    LeftBase = 1024,
    LeftAdded = 2048,
};

struct Color {
    bool operator<(const Color & c) const
    {
        if (r == c.r)
        {
            if (g == c.g)
            {
                if (b == c.b)
                    return false;
                return b < c.b;
            }
            return g < c.g;
        }
        return r < c.r;
    }

    bool operator==(const Color & c) const
    {
        return c.r == r && c.g == g && c.b == b;
    }

    union {
        struct {float r, g, b; };
        float pixel[3];
    };
};

struct ImagePlane {
    unsigned char * image;
    float * imagef;
};

struct Image {
    Image() : imagef(nullptr) {};

    string filename;
    unsigned char * image;
    float * imagef;

    ImagePlane planes[4];
    int width;
    int height;
    int channels;
    size_t size;
    size_t size_plane;

    void pixel_lab(size_t idx, float out[3]);
    void pixel_rgb(size_t idx, float out[3]);
    void pixel_color(size_t idx);

    void deinterlieve();
    void interlieve();
    void tofloats();
};

class Editor {
public:
    Editor();
    ~Editor();
    int load_image(const string & filename);
    int drop_image(size_t index);
    Image empty_image();
    void discard_image(Image & image);
    int save_image(const string & format, const string & filename);
    const Image * get_image(size_t index) const;

    //MONADIC OPERATIONS
    int negative();
    int threshold(float threshold);
    int brightness(float brightness);
    int contrast(float contrast);
    int gamma(float gamma);
    int nonlincontrast(float contrast);
    int logscale(float logscale);
    int quantize(int quantize);
    int equalize();


    //FFT
    int fft_amplitude();

    //Gauss convolution
    int gauss_convolution(float sigma, size_t max_width);

    //Bilateral filter
    int bilateral_filter(float sigma_spatial, float sigma_intensity, size_t max_width, bool log);

    //Stiching images
    int edit(int x, int y,  unsigned int boundry, unsigned int maxiter);

    //Histogram drawing
    int histogram(const string & filename);
    
    //segmentation/drawing
    int draw_segment(bool interactive, float gamma, float lambda);

protected:
    void shake_stack_down();
    void shake_stack_up();
    void discretize(Image & image);

    Image images[STACKSIZE];
    size_t image_stack_filled;
    unsigned char * histoimage;
};