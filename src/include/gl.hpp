#pragma once
#include <GL/gl.h>
#include <vector>
#include <unordered_map>

using namespace std;


void initGL();
void finalizeGL();

struct Program
{
    GLuint id;
    unordered_map<string, GLuint> attr;
    unordered_map<string, GLuint> unif;
};


struct EditProgram : public Program {
    EditProgram();
    ~EditProgram();
    void process(float * image, float * laplace, unsigned int width, unsigned int height, unsigned int maxiter);
    float diff(float * img, float * copy, unsigned int w, unsigned int h);

    GLuint ibuffer, lbuffer, cbuffer;
};


#define ITERMAX 500000
#define FOLD 1000
#define THRESHOLD 1
