#include <cmath>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <GL/gl.h>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cstring>
#include <iostream>
#include "include/gl.hpp"

using namespace std;

void initGL()
{
    //Initn GLEW
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);  
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); 
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);

    GLFWwindow* pWindow = glfwCreateWindow(1, 1, "win", NULL, NULL); // headles mode
    if (!pWindow)
    {
        cerr << "Failed in create window" << endl;
        glfwTerminate();
        return;
    }
    glfwMakeContextCurrent(pWindow);

    glewExperimental = GL_TRUE;
    GLenum err = glewInit();

    if (err != GLEW_OK)
    {
        // Problem: glewInit failed, something is seriously wrong.
        cout << "glewInit failed: " << glewGetErrorString(err) << endl;
        exit(1);
    }
    cout << "Using GLEW Version: " << glewGetString(GLEW_VERSION) << endl;

    GLint inv;
    glGetIntegerv(GL_MAX_COMPUTE_WORK_GROUP_INVOCATIONS, &inv);
    cout << "max invocations per group " << inv << endl;
}

void finalizeGL()
{
    glfwTerminate();
}

void getGLError(const char *desc)
{
    GLenum error = glGetError();

    if (error != 0)
    {
        cout << desc << " " << error << endl;
        cout << gluErrorString(error) << endl;
    }
}

void loadShader(const char *path, string &contents)
{
    //cout << "loading " << path << endl;
    
    ifstream file(path);
    stringstream buffer;

    buffer << file.rdbuf();
    contents = buffer.str();
}

GLuint loadCompileShader(const char *path, GLenum shaderType)
{
    string contents;
    loadShader(path, contents);

    const char *src = &(contents[0]);

    GLuint shader = glCreateShader(shaderType);
    glShaderSource(shader, 1, &src, NULL);
    glCompileShader(shader);

    GLint test;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &test);
    if (!test)
    {
        char compilation_log[512];
        glGetShaderInfoLog(shader, 512, NULL, compilation_log);
        cerr << "Shader compilation ERROR: " << compilation_log << endl;

        //TODO not okay solution
        exit(1);
    }
    else
    {
        //cout << "Compilation OK" << endl;
    }

    return shader;
}

// Create a program from two shaders
GLuint createProgram(const char *path_vert_shader, const char *path_frag_shader)
{
    GLuint vertexShader = loadCompileShader(path_vert_shader, GL_VERTEX_SHADER);
    GLuint fragmentShader = loadCompileShader(path_frag_shader, GL_FRAGMENT_SHADER);

    GLuint shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    // Flag the shaders for deletion
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    // Link and use the program
    glLinkProgram(shaderProgram);

    GLint isLinked = 0;
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, (int *)&isLinked);

    if (isLinked == GL_FALSE)
    {
        char link_log[512];
        glGetProgramInfoLog(shaderProgram, 512, NULL, link_log);
        cout << "Program linking ERROR: " << link_log << endl;
        exit(1);
    }
    else
    {
        //cout << "Linking OK" << endl;
    }

    return shaderProgram;
}

GLuint createProgram(const char *path_compute_shader)
{
    GLuint compute = loadCompileShader(path_compute_shader, GL_COMPUTE_SHADER);

    GLuint program = glCreateProgram();
    glAttachShader(program, compute);
    glLinkProgram(program);

    // Flag the shaders for deletion
    glDeleteShader(compute);

    // Link and use the program
    glLinkProgram(program);

    GLint isLinked = 0;
    glGetProgramiv(program, GL_LINK_STATUS, (int *)&isLinked);

    if (isLinked == GL_FALSE)
    {
        char link_log[512];
        glGetProgramInfoLog(program, 512, NULL, link_log);
        cout << "Program linking ERROR" << link_log << endl;
        exit(1);
    }
    else
    {
        //cout << "Linking OK" << endl;
    }

    return program;
}


EditProgram::EditProgram() : Program()
{
    id = createProgram("src/edit.glsl");
    unif["width"] = glGetUniformLocation(id, "width");
    unif["height"] = glGetUniformLocation(id, "height");
}

EditProgram::~EditProgram()
{
    glDeleteProgram(id);
}

void EditProgram::process(float * image, float * laplace, unsigned int width, unsigned int height, unsigned int maxiter)
{
    float * img_copy = new float[width * height];
    size_t size = width * height * sizeof(float);
    memcpy((void *) img_copy, (const void *) image, size);
    
    //buffers + data copy
    //currently processed image buffer
    glGenBuffers(1, &ibuffer);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, ibuffer);
    glBufferData(GL_SHADER_STORAGE_BUFFER, size, image, GL_DYNAMIC_COPY);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, ibuffer); // buffer on binding 0

    //laplacian buffer
    glGenBuffers(1, &lbuffer);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, lbuffer);
    glBufferData(GL_SHADER_STORAGE_BUFFER, size, laplace, GL_DYNAMIC_COPY);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, lbuffer); // buffer on binding 1

    //copy buffer
    glGenBuffers(1, &cbuffer);
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, cbuffer);
    glBufferData(GL_SHADER_STORAGE_BUFFER, size, nullptr, GL_DYNAMIC_COPY);
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, cbuffer); // buffer on binding 2

    getGLError("buffers");

    //uniforms
    glUseProgram(id);
    glUniform1ui(unif["width"], width);
    glUniform1ui(unif["height"], height);

    //run
    cout << "calculation started" << endl;
    unsigned int groupsx = (width / 32) + ((width % 32) ? 1 : 0);
    unsigned int groupsy = (height / 32) + ((height % 32) ? 1 : 0);
    float difff = 1;
    size_t i = 0;

    //stop when diff is less than THRESHOLD or the max number of iterations was exceeded
    while((difff * 255) > THRESHOLD && i < maxiter)
    {
        //see edit.glsl for the compute shader details
        glDispatchCompute(groupsx, groupsy, 1);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);
        glCopyNamedBufferSubData(cbuffer, ibuffer, 0, 0, size);
            
        //each fold, test how much the image changed
        //if the change is less than THRESHOLD level of intensity
        //end
        if (i % FOLD == 0)
        {
            glFinish(); // force sync every 1000 iterations!!!
            difff = diff(image, img_copy, width, height);
            cout << difff * 255 << " " << i << "\n" << flush;
        }  
        i++;  
    }

    //already copied to CPU inside the diff
    //finalize run
    glDeleteBuffers(1, &ibuffer);
    glDeleteBuffers(1, &lbuffer);
    delete [] img_copy;
    glFinish();
    getGLError("end");

}

float EditProgram::diff(float * img, float * copy, unsigned int w, unsigned int h)
{
    //compute difference of the current and previus fold
    //could be done again on gpu, here im using just buffers on cpu side
    glBindBuffer(GL_SHADER_STORAGE_BUFFER, ibuffer);
    float * gpuImage = (float *) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0, w * h * sizeof(float), GL_MAP_READ_BIT);
    memcpy((void *)img, (const void *) gpuImage, w * h * sizeof(float));
    glUnmapBuffer(GL_SHADER_STORAGE_BUFFER);
    glFinish();


    float diff = 0;
    for (size_t i = 0; i < w * h; i++)
    {
        diff = max(diff, fabs(img[i] - copy[i]));
        copy[i] = img[i];
    }

    return diff;
}





