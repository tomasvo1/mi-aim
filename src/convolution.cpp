#include <iostream>
#include <cmath>
#include <cstring>
#include "include/editor.hpp" 

using namespace std;

int Editor::gauss_convolution(float sigma, size_t max_width){
    Image & image = images[0];

    if(image.image == nullptr)
        return EMPTYIMAGESLOT;

    int pos, kpos;
    float sigma2 = sigma * sigma;
    float * tmpdata = new float[image.width * image.height];

    //prepare the kernel
    int ksize = min((size_t) 2 * image.width, max_width);
    ksize += ksize % 2 ? 0 : 1; //set ksize odd
    cout << "kernel size: " << ksize << endl;
    float * gaussian = new float[ksize]; // the actuall kernel

    float invfac = 1 / sqrt(2 * M_PI * sigma2);
    for (int i = 0; i < ksize; i++)
    {
        kpos = i - (ksize / 2);
        gaussian[i] = invfac * exp(-(kpos * kpos) / (2 * sigma2));
    }

    memset(tmpdata, 0, image.width * image.height * sizeof(float));

    float pixel_accu;
    //the convolution in the horizontal direction
    for (size_t c = 0; c < image.channels; c++)
    { 
        for (int i = 0; i < image.height; i++)
        {
            for (int j = 0; j < image.width; j++)
            {
                pixel_accu = 0;
                for (int k = 0; k < ksize; k++)
                {
                    //position inside kernel
                    kpos = k - (ksize / 2);
                    //position in the image on the row i
                    //taking into account mirroring of the image
                    pos = abs(j + kpos); // left border
                    pos -= 2 * max(0, pos - (image.width - 1)); //right border

                    //position in the image
                    pos = i * image.width + pos;

                    //add wighted value into accumulator
                    pixel_accu += image.planes[c].imagef[pos] * gaussian[k];
                }
                tmpdata[i * image.width + j] = pixel_accu;
            }
        }

        //the convolution in the vertical direction
        for (int i = 0; i < image.width; i++)
        {
            for (int j = 0; j < image.height; j++)
            {
                pixel_accu = 0;
                for (int k = 0; k < ksize; k++)
                {
                    kpos = k - (ksize / 2);

                    //coordinate inside active column
                    pos = abs(j - kpos);
                    pos -= 2 * max(0, pos - (image.height - 1));

                    //recalc
                    pos = pos * image.width + i;
                    pixel_accu += tmpdata[pos] * gaussian[k];
                }
                image.planes[c].imagef[j * image.width + i] = pixel_accu;
            }
        }
    }

    delete [] gaussian;
    delete [] tmpdata;

    return OPOK;
}

