#include <iostream>
#include <cmath>
#include <cstring>
#include "include/editor.hpp" 

using namespace std;


float * setup_kernel(float sigma, size_t max_width, int & ksize)
{
    int kpos;
    float sigma2 = sigma * sigma;
    //prepare the kernel
    ksize = max_width;
    ksize += ksize % 2 ? 0 : 1; //set ksize odd
    float * gaussian = new float[ksize]; // the actuall kernel

    float invfac = 1 / sqrt(2 * M_PI * sigma2);
    for (int i = 0; i < ksize; i++)
    {
        kpos = i - (ksize / 2);
        //gaussian[i] = invfac * exp(-(kpos * kpos) / (2 * sigma2));
        gaussian[i] = exp(-(kpos * kpos) / (2 * sigma2));
    }

    return gaussian;
}

float gaussian(float x, float invsigma2, float sigma2)
{
    //return invsigma2 * exp(-(x * x) / (2 * sigma2));
    return exp(-(x * x) / (2 * sigma2));
}

int Editor::bilateral_filter(float sigma_spatial, float sigma_intensity, size_t max_width, bool log)
{
    Image & image = images[0];

    //sigma_intensity /= 255;

    if(image.image == nullptr)
        return EMPTYIMAGESLOT;

    int ksize; //spatial kernel size]
    //gaussian spatial kernel
    float * skernel = setup_kernel(sigma_spatial, max_width, ksize);
    //tmp data
    float * tmpdata = new float[image.size];


    float pixel_accu, cpx, lpx, w, lw;
    float isigma2 = sigma_intensity * sigma_intensity;
    int rpos, cpos, pos;

    float invsigma2 = 1 / sqrt(2 * M_PI * isigma2), dist;

    float totalop = image.size;
    float fprog = 0;

    size_t progress = 0, iter = 0;

    //the actual filtration
    for (size_t c = 0; c < image.channels; c++)
    {

        for (int i = 0; i < image.height; i++)
        {
            for (int j = 0; j < image.width; j++)
            {
                pixel_accu = 0;
                cpx = image.planes[c].imagef[i * image.width + j];
                w = 0;

                //iterate over elemens of the kernel
                for (int k = 0, kpos = -(ksize / 2); k < ksize; k++, kpos++)
                {
                    for (int l = 0, lpos = -(ksize / 2); l < ksize; l++, lpos++)
                    {
                        //select right row
                        //coordinate inside active column
                        cpos = abs(i + kpos); //top border
                        cpos -= 2 * max(0, cpos - (image.height - 1)); //bottom border
                        cpos *= image.width;

                        //select right column
                        rpos = abs(j + lpos); // left border
                        rpos -= 2 * max(0, rpos - (image.width - 1)); //right border

                        //position in the image
                        pos = cpos + rpos;
                        lpx = image.planes[c].imagef[pos];

                        if (log)
                            dist = logf(lpx / cpx); 
                        else
                            dist = lpx - cpx;

                        lw = skernel[k] * skernel[l] * gaussian(dist, invsigma2, isigma2);
                        pixel_accu += lpx * lw;
                        w += lw;
                    }
                }

                tmpdata[(i * image.width + j) + c * image.size_plane] = pixel_accu / w;
                fprog = (progress++ / totalop) * 100;
                if (fprog > iter) {
                    iter = fprog;
                    cout << iter << "%\r" << flush;
                }
            }
        }
    }


    for (size_t i = 0; i < image.size; i++)
        image.imagef[i] = tmpdata[i];


    delete [] tmpdata;
    delete [] skernel;
    cout << endl;

    return OPOK;
}