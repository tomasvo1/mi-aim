#include <iostream>
#include <string>
#include <sstream>
#include <cstring>

#include "include/interface.hpp"
#include "include/editor.hpp"

#define PROMPT ">>>"
#define EPSILON 10e-20f

using namespace std;

Interface::Interface()
{
    cout << "Initializing the application..." << endl;

    calls["open"] = &Interface::load_image;
    calls["drop"] = &Interface::drop_image;
    calls["save"] = &Interface::save_image;
    calls["help"] = &Interface::print_help;
    calls["images"] = &Interface::print_image_stack;
    calls["negate"] = &Interface::negative;
    calls["threshold"] = &Interface::threshold;
    calls["brightness"] = &Interface::brightness;
    calls["contrast"] = &Interface::contrast;
    calls["gamma"] = &Interface::gamma;
    calls["nonlincontrast"] = &Interface::nonlincontrast;
    calls["logscale"] = &Interface::logscale;
    calls["equalize"] = &Interface::equalize;
    calls["quantize"] = &Interface::quantize;
    calls["histogram"] = &Interface::histogram;
    calls["fft"] = &Interface::fft;
    calls["gauss"] = &Interface::gauss;
    calls["bfilter"] = &Interface::bilateral_filter;
    calls["merge"] = &Interface::merge;
    calls["fill"] = &Interface::scribble;
}

void Interface::run()
{

    cout << PROMPT << flush;
    string line, input;

    //main loop until exit
    while (getline(cin, line))
    {
        if (!line.empty())
        {
            istringstream iss(line);
            iss >> input;

            if (input == "exit")
            {
                iss.clear();
                return;
            }

            auto it = calls.find(input);
            if (it != calls.end())
                (this->*(it->second))(iss);

        }

        if (cin.eof())
            return;

        cout << PROMPT << flush;
    }
}

void Interface::load_image(istringstream &iss)
{
    string filename;
    iss >> filename;

    if (filename.empty())
    {
        cout << "command: open <filename>" << endl;
        return;
    }

    int status = editor.load_image(filename);

    if (status != OPOK)
        cout << "there was some error loading the image" << endl;
}

void Interface::drop_image(istringstream &iss)
{
    size_t index;
    if (!(iss >> index))
    {
        cout << "command: drop <stack index>" << endl;
        return;
    }

    int status = editor.drop_image(index);

    if (status != OPOK)
        cout << "there was some error droping the image" << endl;
}

void Interface::save_image(istringstream &iss)
{
    string format, filename;

    if (!(iss >> format >> filename))
    {
        cout << "command: save <png|jpg> <output name>" << endl;
        return;
    }

    int status = editor.save_image(format, filename);

    if (status != OPOK)
        cout << "there was some error loading the image" << endl;
}

void Interface::histogram(istringstream &iss)
{
    string filename;
    int status;
    iss >> filename;
    
    status = editor.histogram(filename);


    if (status != OPOK)
        cout << "there was some error creating the histogram" << endl;
}

void Interface::negative(istringstream & iss)
{
    int status = editor.negative();

    if (status != OPOK)
        cout << "there was some error negating the image" << endl; 
}

void Interface::threshold(istringstream &iss)
{
    float threshold;

    if (!(iss >> threshold))
    {
        cout << "command: threshold <threshold value>" << endl;
        return;
    }

    int status = editor.threshold(min(max(threshold, 0.f), 1.0f));

    if (status != OPOK)
        cout << "there was some error" << endl;
}

void Interface::brightness(istringstream &iss)
{
    float brightness;

    if (!(iss >> brightness))
    {
        cout << "command: brightness <brightness value>" << endl;
        return;
    }

    int status = editor.brightness(min(max(brightness, -1.f), 1.f));

    if (status != OPOK)
        cout << "there was some error" << endl;
}

void Interface::contrast(istringstream &iss)
{
    float contrast;

    if (!(iss >> contrast))
    {
        cout << "command: contrast <contrast value>" << endl;
        return;
    }

    int status = editor.contrast(max(contrast, 0.f));

    if (status != OPOK)
        cout << "there was some error" << endl;
}

void Interface::gamma(istringstream &iss)
{
    float gamma;

    if (!(iss >> gamma))
    {
        cout << "command: gamma <float gamma value>" << endl;
        return;
    }

    int status = editor.gamma(max(gamma, 0.f));

    if (status != OPOK)
        cout << "there was some error" << endl;
}

void Interface::nonlincontrast(istringstream &iss)
{
    float contrast;

    if (!(iss >> contrast))
    {
        cout << "command: nonlincontrast <float contrast value>" << endl;
        return;
    }

    int status = editor.nonlincontrast(min(max(contrast, 0.f), 1.f));

    if (status != OPOK)
        cout << "there was some error" << endl;
}

void Interface::logscale(istringstream &iss)
{
    float logscale;

    if (!(iss >> logscale))
    {
        cout << "command: logscale <float logscale value>" << endl;
        return;
    }

    int status = editor.logscale(max(logscale, -1.f + EPSILON));

    if (status != OPOK)
        cout << "there was some error" << endl;
}

void Interface::quantize(istringstream &iss)
{
    int quantize;

    if (!(iss >> quantize))
    {
        cout << "command: quantize <number of quantize levels>" << endl;
        return;
    }

    int status = editor.quantize(max(quantize, 1));

    if (status != OPOK)
        cout << "there was some error" << endl;
}

void Interface::equalize(istringstream & iss)
{
    int status = editor.equalize();

    if (status != OPOK)
        cout << "there was some error" << endl;
}

void Interface::fft(istringstream & iss)
{
    int status = editor.fft_amplitude();

    if (status != OPOK)
        cout << "there was some error" << endl;
}

void Interface::gauss(istringstream & iss)
{
    float sigma;
    size_t max_width;

    if (!(iss >> sigma))
    {
        cout << "command: gauss <sigma> (max kernel width, factor of sigma)" << endl;
        return;
    }

    if (!(iss >> max_width))
        max_width = 6 * sigma; //to get max
    else 
        max_width = max_width * sigma;

    int status = editor.gauss_convolution(sigma, max_width);

    if (status != OPOK)
        cout << "there was some error" << endl;
}

void Interface::bilateral_filter(istringstream & iss)
{
    float sigmas, sigmai;
    int log;
    size_t max_width;

    if (!(iss >> sigmas >> sigmai >> log))
    {
        cout << "command: bfilter <sigma spatial> <sigma intensity> <log 0/1> (max kernel width, factor of spatial sigma)" << endl;
        return;
    }

    if (!(iss >> max_width))
        max_width = 6 * sigmas; //to get max
    else 
        max_width = max_width * sigmas;

    int status = editor.bilateral_filter(sigmas, sigmai, max_width, log);

    if (status != OPOK)
        cout << "there was some error" << endl;
}

void Interface::merge(istringstream & iss)
{
    int x, y;
    size_t max_width;

    if (!(iss >> x >> y))
    {
        cout << "command: merge <x-shift of top image> <y-shift of top image>" << endl;
        return;
    }

    //top
    unsigned int border = 0;
    unsigned char bor;
    if (iss >> bor)
    {
        if (bor == 'B')
            border |= MergeMask::TopBase;
        else if (bor == 'A')
            border |= MergeMask::TopAdded;
        else
            border |= MergeMask::TopNone;

    }

    //right
    if (iss >> bor)
    {
        if (bor == 'B')
            border |= MergeMask::RightBase;
        else if (bor == 'A')
            border |= MergeMask::RightAdded;
        else
            border |= MergeMask::RightNone;

    }

    //bottom
    if (iss >> bor)
    {
        if (bor == 'B')
            border |= MergeMask::BottomBase;
        else if (bor == 'A')
            border |= MergeMask::BottomAdded;
        else
            border |= MergeMask::BottomNone;

    }

    //left
    if (iss >> bor)
    {
        if (bor == 'B')
            border |= MergeMask::LeftBase;
        else if (bor == 'A')
            border |= MergeMask::LeftAdded;
        else
            border |= MergeMask::LeftNone;    
    }

    //maxiter
    unsigned int maxiter;
    if (!(iss >> maxiter))
    {
        maxiter = 500000; // default number of max iterations
    }

    int status = editor.edit(x, y, border, maxiter);

    if (status != OPOK)
        cout << "there was some error" << endl;
}


void Interface::scribble(istringstream & iss)
{
    int interactive = 0;
    float gamma = 1;
    float lambda = 0.05;

    if (!(iss >> gamma))
    {
        cout << "drawing: drawing <gamma> (lambda = 0.05)" << endl;
        return;
    }

    if (!(iss >> lambda))
    {
        lambda = 0.05;
    }



    int status = editor.draw_segment(interactive, gamma, lambda);

    if (status != OPOK)
        cout << "there was some error" << endl;
}


void Interface::print_help(istringstream & iss)
{
    cout << "Overview of available commands:\n"
            "    command <parameter> (optional parameter)\n\n"
            "    open <filename>\n"
            "           loads image from file onto image stack\n\n"
            "    drop <stack index>\n"
            "           drop first image with corresponding\n"
            "           filename from image stack\n\n"
            "    images\n"
            "          list all images loaded on image stack\n\n"
            "    save <png|jpg> <output name>\n"
            "          save image on top of image stack\n"
            "          into a file with chosen format\n\n"
            "    histogram (filename)\n"
            "          draw histogram of the image on top of the stack\n"
            "          or save the histogram it into .png file\n\n"
            "    negate\n"
            "          negate the image on top of the image stack\n\n"
            "    threshold <value from <0, 1>>\n"
            "          threshold the image on top of the image stack\n\n"
            "    brightness <value from <-1, 1>>\n"
            "          adjust brightness of the iamge\n"
            "          on top of the image stack\n\n"
            "    contrast <value from <0, inf)>\n"
            "          adjust the contrast of the image\n"
            "          on top of the image stack\n\n"
            "    gamma <value from <0, 1>>\n"
            "          gamma correction of the image\n"
            "          on top of the image stack\n\n"
            "    nonlincontrast <value from <0, 1>>\n"
            "          nonlinear contrast correction of the image\n"
            "          on top of the image stack\n\n"
            "    logscale <value from (-1, inf)>\n"
            "          logaritmical scale of the image\n"
            "          on top of the image stack\n\n"
            "    quantize <integer value from <1, inf>>\n"
            "          quantize the image\n"
            "          on top of the image stack\n\n"
            "    equalize\n"
            "          eqalize the image\n"
            "          on top of the image stack\n\n"
            "    fft\n"
            "          get amplitude spectrum of the image\n"
            "          on top of the image stack\n\n"
            "    gauss <sigma> (max kernel width, factor of sigma)\n"
            "          appies separable convolution to the image\n"
            "          on top of the image stack\n"
            "          kernel width is computed as n * sigma with default n = 6\n"
            "          and the factor n can be changed as a second argument\n\n"
            "    bfilter <sigma spatial> <sigma intensity> <log 0/1> (max kernel width, factor of spatial sigma)\n"
            "          applies bilateral filter to the top image\n"
            "          kernel width is computed with n * sigma_spatial where default n = 6\n"
            "          and the factor n can be changed with the fourth optional parameter\n\n"
            "    exit\n"
            "          exit the application\n\n";
}

void Interface::print_image_stack(istringstream & iss) {
    for (size_t i = 0; i < STACKSIZE; i++)
    {
        const Image * image = editor.get_image(i);
        if (image == nullptr)
        {
            if (i == 0)
            cout << "    no images" << endl;
            return; 
        }

        cout << "    " << i << ": " << image->filename << " : " << image->width << "x" << image->height << endl; 
    }
}