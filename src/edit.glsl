#version 430 core

//size of a local workgroup
layout (local_size_x = 32, local_size_y = 32) in;


//BUFFERS AND UNIFORMS
layout(std430, binding = 0) buffer bimage
{
    float image[];
};

layout(std430, binding = 1) buffer blaplace
{
    float laplace[];
};

layout(std430, binding = 2) buffer bcopy
{
    float copy[];
};

layout(location = 1) uniform uint width;
layout(location = 2) uniform uint height;


shared float local_frame[1156]; //size 34 * 34


void main(void) 
{
    //copy from global memory
    //currently processed pixel
    uint x = gl_WorkGroupID.x * gl_WorkGroupSize.x + gl_LocalInvocationID.x;
    uint y = gl_WorkGroupID.y * gl_WorkGroupSize.y + gl_LocalInvocationID.y;
    uint lw = gl_WorkGroupSize.x + 2;

    //check if we are still inside image
    if (x >= width || y >= height)
        return;

    uint local = (gl_LocalInvocationID.y + 1) * lw + (gl_LocalInvocationID.x + 1);
    uint global = y * width + x;

    //copy the global to local
    local_frame[local] = image[global];

    //left
    if (gl_LocalInvocationID.x == 0)
    {
        if (x != 0)
            local_frame[local - 1] = image[global - 1];
        else
            local_frame[local - 1] = 0;
    }

    //right
    if (gl_LocalInvocationID.x == gl_WorkGroupSize.x - 1 || x == width - 1)
    {
        if (x != width - 1)
            local_frame[local + 1] = image[global + 1];
        else 
            local_frame[local + 1] = 0;
    }       

    //top
    if (gl_LocalInvocationID.y == 0)
    {
        if (y != 0)
            local_frame[local - lw] = image[global - width];
        else
            local_frame[local - lw] = 0;
    }

    //bottom
    if (gl_LocalInvocationID.y == gl_WorkGroupSize.y - 1 || y == height - 1)
    {
        if (y != height - 1)
            local_frame[local + lw] = image[global + width];
        else
            local_frame[local + lw] = 0;
    }

    //wait till all local writes are complete
    barrier();

    //compute and saveback
    copy[global] = 0.25 * (
        local_frame[local - lw] + local_frame[local + lw] + local_frame[local - 1] + local_frame[local + 1]
        + laplace[global]); 

    
}