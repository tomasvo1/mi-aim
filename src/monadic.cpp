#include <iostream>
#include <cmath>
#include "include/editor.hpp" 

using namespace std;


//MONADIC OPERATIONS
int Editor::negative()
{
    Image & image = images[0];

    if(image.imagef == nullptr)
        return EMPTYIMAGESLOT;

    for (size_t i = 0; i < image.size; i++)
        image.imagef[i] = 1.0f - image.imagef[i];

    return OPOK; 
}

int Editor::threshold(float threshold)
{
    Image & image = images[0];

    if(image.imagef == nullptr)
        return EMPTYIMAGESLOT;

    for (size_t i = 0; i < image.size; i++)
        image.imagef[i] = image.imagef[i] >= threshold ? 1.f : 0.f;

    return OPOK; 
}

int Editor::brightness(float brightness)
{
    Image & image = images[0];

    if(image.imagef == nullptr)
        return EMPTYIMAGESLOT;

    for (size_t i = 0; i < image.size; i++)
        image.imagef[i] = max(min(image.imagef[i] + brightness, 1.f), 0.f);

    return OPOK; 
}

int Editor::contrast(float contrast)
{
    Image & image = images[0];

    if(image.imagef == nullptr)
        return EMPTYIMAGESLOT;

    for (size_t i = 0; i < image.size; i++)
        image.imagef[i] = max(min(image.imagef[i] * contrast, 1.f), 0.f);

    return OPOK; 
}

int Editor::gamma(float gamma)
{
    Image & image = images[0];

    if(image.imagef == nullptr)
        return EMPTYIMAGESLOT;

    for (size_t i = 0; i < image.size; i++)
        image.imagef[i] = pow(image.imagef[i], gamma);

    return OPOK; 
}

int Editor::nonlincontrast(float contrast)
{
    Image & image = images[0];

    if(image.imagef == nullptr)
        return EMPTYIMAGESLOT;

    float gamma = 1 / (1 - contrast);

    for (size_t i = 0; i < image.size; i++)
    {
        if (image.imagef[i] < 0.5)
            image.imagef[i] = 0.5 * pow((2 * image.imagef[i]), gamma);
        else
            image.imagef[i] = 1.0 - 0.5 * pow(2 * (1.0 - image.imagef[i]), gamma);
    }

    return OPOK; 
}

int Editor::logscale(float logscale)
{
    Image & image = images[0];

    if(image.image == nullptr)
        return EMPTYIMAGESLOT;

    for (size_t i = 0; i < image.size; i++)
        image.imagef[i] = logf(1.f + image.imagef[i] * logscale) / logf(1.f + logscale);
        
    return OPOK; 
}

int Editor::quantize(int quantize)
{
    Image & image = images[0];

    if(image.image == nullptr)
        return EMPTYIMAGESLOT;

    for (size_t i = 0; i < image.size; i++)
        image.imagef[i] = floor(image.imagef[i] * quantize) / quantize;

    return OPOK; 
}

int Editor::equalize()
{
    Image & image = images[0];

    if(image.image == nullptr)
        return EMPTYIMAGESLOT;

    for (size_t c = 0; c < image.channels; c++)
    {
        size_t histo[256];
        float cdf[256];
        size_t cdfacu = 0;

        for (size_t i = 0; i < 256; i++)
            histo[i] = 0;

        discretize(image);

        //accumulate
        for (size_t i = 0; i < image.size; i++)
            histo[image.planes[c].image[i]]++;

        //get cdf
        for (size_t i = 0; i < 256; i++)
        {
            cdfacu += histo[i];
            cdf[i] = (float) cdfacu / (float) image.size;
        }

        cout << cdfacu << " " << image.size << endl;

        //equalize
        for (size_t i = 0; i < image.size; i++)
            image.planes[c].imagef[i] = cdf[image.planes[c].image[i]];
    }
    


    return OPOK; 
}