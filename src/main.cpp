#include "include/interface.hpp"

int main(int argc, char const *argv[])
{
    //init application interface
    Interface interface;

    //run the interface
    interface.run();
    return 0;
}
