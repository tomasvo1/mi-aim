# Seperabilní konvoluce
Implementace je v souboru [convolution.cpp](../src/convolution.cpp) v metodě `gauss_convolution`. Zvolil jsem následující postup:

1) Nejprve odhadnu šířku 1D jádra pro daný rozptyl, abych si mohl jádro předpočítat a nepočítal ho pořád dokola. Možná šířku zbytečně nadsadím, jelikož údajně pokud si ho chci předpočítat, tak dle [wikipedie](https://en.wikipedia.org/wiki/Gaussian_blur) stačí šířka o velikosti cca 6 sigma, nadsadil jsem ho raději na 12 sigma, s tím že se ten násobek dá ručně zvolit na vstupu jako nepovinný parametr.

2) Vyrobím si vzorky 1D gaussovského jádra o požadované šířce s požadovaným rozptylem.

3) Provedu 1D konvoluci v horizontálním směru pro každý pixel (posouvám plovoucí okénko které má výšku jeden řádek a šířku kterou jsem odhadl výše), uložím do dočasného pole.

4) Provedu 1D konvoluci v druhém směru nad dočasným polem (okénko má šířku jeden sloupec a výšku odhadnutou výše, je to to samé jádro, jen se na něj dívam jinak), výsledek uložím do původního pole.

Složitost výpočtu sedí na asymtotický odhad z přednášky. Alternativně bych si mohl ušetřit předvýpočet jádra a vyhodnocovat ho opakovaně pro každý pixel, to by bylo ale časově náročnější. Zároveň původní obraz indexuju tak aby po okrajích došlo k zrcadlení, jako na obrázku níže:

<img src="mirror.png" width="300">

Ukázka výstupů:

    open lena.png
    gauss 5 #a pak 10, 15 a 20
    save png lena05

první obrázek je originál, na ostatní byl aplikováno gaussovské jádro s rozptyly 5, 10, 15 a 20

<img src="lena.png" width="150">
<img src="lena05.png" width="150">
<img src="lena10.png" width="150">
<img src="lena15.png" width="150">
<img src="lena20.png" width="150">


    open cica.png
    gauss 5
    save png cica05

první obrázek je originál, na ostatní byl aplikováno gaussovské jádro s rozptyly 5, 10, 50 a 100

<img src="cica.png" width="150">
<img src="cica05.png" width="150">
<img src="cica10.png" width="150">
<img src="cica50.png" width="150">
<img src="cica100.png" width="150">


    open duck.png
    gauss 10
    save png duck10

první obrázek je originál, na ostatní byl aplikováno gaussovské jádro s rozptyly 10, 20, 30 a 40

<img src="duck.png" width="150">
<img src="duck10.png" width="150">
<img src="duck20.png" width="150">
<img src="duck30.png" width="150">
<img src="duck40.png" width="150">

## Experimenty s předpočítáním jádra
Zkusil jsem experimentovat s omezením rozsahu předpočítaného jádra, prakticky čekám, že se obraz bude chovat, jako kdybych za sebe skládal dvě konvoluce a jádro ořiznul box filtrem, následuje hokus pokus:

Zvolil jsem si pevnou sigmu 10 a otestoval jádra s různou šířkou. K mému překvapení (nic nečekaného to není, ale stejně jsem si to neuvědomil) vyšel následující výsledek:

snímky s předpočítaným jádrem s šířkou 1 * sigma, 2 * sigma, 3 * sigma, 6 * sigma, 9 * sigma, 12 * sigma a 18 * sigma

<img src="lena1.png" width="100">
<img src="lena2.png" width="100">
<img src="lena3.png" width="100">
<img src="lena6.png" width="100">
<img src="lena12.png" width="100">
<img src="lena18.png" width="100">

Oříznutí box filterm se nekoná, jelikož jádro není znormalizované. Výsledky použití jádra s malou šířkou jsou tmavé, samozřejmě, jelikož ořezané jádro není znormalizované, a tak chybí příspěvky od ořezaných hodnot, kterých bylo patrně více. Zároveň za povšimnutí stojí, že výsledky od 6 * sigma doprava jsou na první pohled nerozeznatelné (wikipedie zřejmě měla tentokrát pravdu). Obrázek níže je rozdíl obrazů pro sigma = 10 s šířkou jádra sigma * 6 a sigma * 18:

<img src="lenadiff.png" width="150">

což je naprosto černý snímek. Šířka 6 * sigma tedy dost pravděpdobně stačí a jakákoliv větší šířka už žádnou novou informaci nepřinese.





