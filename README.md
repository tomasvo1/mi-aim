# Domácí úkoly MI-AIM
Zdrojové kódy jsou ve složce `src`, úkoly jsou psané v C++, pro načítání a ukládání obrázků je použita knihovna STB (vybrané části). 

## 1. úkol - Monadické operace
Výsledky jsou uloženy ve [složce monadic](./monadic) spolu s podrobnějším popisem, byla přidána demonstrace s lepším obrázkem.

Řešení domácích úkolů je koncipované jako jednoduchý commandline editor, do kterého budou postupně přidávány jednotlivé funkcionality. 

## 2. úkol - Fourierova transformace
Ukázky výstupů jsou ve [složce fourier](./fourier). Implementace je provedena pomocí FFTW.


## 3. úkol - Konvoluce
Ukázky výstupů jsou ve [složce convolution](./convolution).

## 4. úkol - Bilaterální filtr
Ukázky výstupů jsou ve [složce bilateral](./bilateral). Dodatečně jsem zkoušel experimentovat s normalizací a barvou, výsledky uvnitř.

## 5. úkol - Editace obrazu
Ukázky výstupů jsou ve [složce edit](./edit). Dodatečně se snažím přijít, co způsobuje barevnou aberaci v prvním obrázku, naplánováno jako práce na víkend.

## 6. úkol - Segmentace obrazu
Ukázky výstupů jsou ve [složce drawing](./drawing). 

## Kompilace
Je přiložen Makefile, vše by mělo fungovat na všech platformách (snad), jiné závislosti nejsou třeba. Vyvíjím na Linuxu. 

## Ovládání

    Overview of available commands:
    
    command <parameter> (optional parameter)

    open <filename>
           loads image from file onto image stack

    drop <stack index>
           drop first image with corresponding
           filename from image stack

    images
          list all images loaded on image stack

    save <png|jpg> <output name>
          save image on top of image stack
          into a file with chosen format

    histogram (filename)
          draw histogram of the image on top of the stack
          or save the histogram it into .png file

    negate
          negate the image on top of the image stack

    threshold <value from <0, 1>>
          threshold the image on top of the image stack

    brightness <value from <-1, 1>>
          adjust brightness of the iamge
          on top of the image stack

    contrast <value from <0, inf)>
          adjust the contrast of the image
          on top of the image stack

    gamma <value from <0, 1>>
          gamma correction of the image
          on top of the image stack

    nonlincontrast <value from <0, 1>>
          nonlinear contrast correction of the image
          on top of the image stack

    logscale <value from (-1, inf)>
          logaritmical scale of the image
          on top of the image stack

    quantize <integer value from <1, inf>>
          quantize the image
          on top of the image stack

    equalize
          eqalize the image
          on top of the image stack

    fft
          get amplitude spectrum of the image
          on top of the image stack

    gauss <sigma> (max kernel width, factor of sigma)
          appies separable convolution to the image
          on top of the image stack
          kernel width is computed as n * sigma with default n = 6
          and the factor n can be changed as a second argument

    bfilter <sigma spatial> <sigma intensity> <log 0/1> (max kernel width, factor of spatial sigma)
          applies bilateral filter to the top image
          kernel width is computed with n * sigma_spatial where default n = 6
          and the factor n can be changed with the fourth optional parameter    
    
    exit
          exit the application
