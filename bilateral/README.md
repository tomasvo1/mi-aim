# Bilaterální filter
Implementace je v souboru [bilateral.cpp](../src/bilateral.cpp). Filter je implementován bez jakýchkoliv zrychlení, Gaussovské jádro prostorového filtru je předpočítáno v jednom rozměru a za běhu je opakovaně počítána hodnota 2D jádra pronásobením příslušných hodnot - je to sice zpomalení, ale zato je použito méně paměti (což u malých jader kde `|F| >> |G|` nehraje takovou roli, výpočetně je tohle provedení vlastně horší...). Šířka jádra je odhadnuta na základě postorového rozptylu stejně jako v předchozí části. V implementaci je `if` pro výpočet členu b z rozdílu nebo logaritmu podílu hodnot, dá se konfigurovat na vstupu (jako vždy). Implementace počítá metodou brute-force. 

Jeden problém, na který jsem v průběhu implementace narazil bylo škálování jádra aplikovaného na jasové hodnoty. V počátku jsem si neuvědomil, že pokud budu používat jádro s rozptylem v řádu jednotek, či desítek, a zároveň budu používat interní obrazovou reprezentaci jasu ve floatech s rozmezím hodnot `0-1`, pak nebude mít člen `b` prakticky žádný vliv, bude prakticky pro všechny hodnoty intenzity stejný. Jako řešení se nabízely tři možnosti

* používat rozptyl řádu desetin nebo setin
* vynásobit dočasně hodnoty v obraze `255` a používat rozptyl v řádu jednotek/desítek
* vydělit rozptyl &sigma;<sub>b</sub> (zadaný v řádu desítek/jednotek) `255` a "přeškálovat" ho tak pro hodnoty do rozsahu `0-1`

První možnost by fungovala není potřeba ji nějak moc obhajovat, jádro bude reagovat nejvíce na jasové hodnoty lišící se o hodnotu uvnitř intervalu vymezeného rozptylem, rozdíly hodnot jsou v řádu desetin/setin, rozptyl také, vše funguje.

Druhý postup bude také fungovat, do gaussianu `b` dosadím rozptyl v řádu desítek, to samé platí pro dosazené jasové hodnoty, na konci se obraz znormalizuje a nezáleží na tom v jakém rozmezí jsou hodnoty ve zbytku výpočtu.

Třetí postup bude také fungovat, jelikož ve výpočtu gaussianu `b` vlastně exponent násobím 255<sup>2</sup> což je analogické předchozímu případu. Navíc je ale celý gaussian `b` násoben 255 (kvůli sigmě dole ve jmenovateli před `e`), ale jelikož všechny `b` jsou násobeny 255 a na konci normalizujeme, tak to také není problém. 

Tím pádem by mělo být jedno jaký postup zvolím, vybral jsem si ten třetí. Jde jen o to, nějak srovnat rozptyl gaussianu aplikovaného na jasové hodnoty a rozsah hodnot v obrázku.

---

Hledal jsem nějaký ideálně zašumělý obrázek pro demonstraci, nakonec jsem použil [tento](https://cs.wikipedia.org/wiki/Soubor:Historical_picture_of_today%27s_street_1._m%C3%A1je_no._800.jpg). Ukázka níže používá výpočet pomocí rozdílu jasových hodnot.


<img src="histo.png" width="300">

    open bilateral/histo.png
    bfilter 10 5 0
    save png bilateral/histo10_5


|   | &sigma;<sub>b</sub> = 1 | &sigma;<sub>b</sub> = 5  |  &sigma;<sub>b</sub> = 10 | &sigma;<sub>b</sub> = 50  | &sigma;<sub>b</sub> = 150  |
|---|---|---|---|---|---|
| &sigma;<sub>G</sub> = 1 | <img src="histo1_1.png" width="300"> | <img src="histo1_5.png" width="300"> | <img src="histo1_10.png" width="300"> | <img src="histo1_50.png" width="300"> | <img src="histo1_150.png" width="300"> |
| &sigma;<sub>G</sub> = 10 | <img src="histo10_1.png" width="300"> | <img src="histo10_5.png" width="300"> | <img src="histo10_10.png" width="300"> | <img src="histo10_50.png" width="300"> | <img src="histo10_150.png" width="300"> |
| &sigma;<sub>G</sub> = 40 | <img src="histo40_1.png" width="300"> | <img src="histo40_5.png" width="300"> | <img src="histo40_10.png" width="300"> | <img src="histo40_50.png" width="300"> | <img src="histo40_150.png" width="300"> |

Ukázka číslo dvě, tentokrát s logaritmem podílu, použitý obrázek [pochází odtud](https://www.pexels.com/photo/close-up-photo-of-girl-wearing-white-headscarf-2598024/).

<img src="girl.png" width="300"> 

    open bilateral/girl.png
    bfilter 10 20 1
    save png bilateral/girl10_20


|   | &sigma;<sub>b</sub> = 1 | &sigma;<sub>b</sub> = 5  |  &sigma;<sub>b</sub> = 10 | &sigma;<sub>b</sub> = 20  | &sigma;<sub>b</sub> = 40  | &sigma;<sub>b</sub> = 40 | 
|---|---|---|---|---|---|---|
| &sigma;<sub>G</sub> = 1 | <img src="girl1_1.png" width="300"> | <img src="girl1_5.png" width="300"> | <img src="girl1_10.png" width="300"> | <img src="girl1_20.png" width="300"> | <img src="girl1_40.png" width="300"> | <img src="girl1_60.png" width="300"> |
| &sigma;<sub>G</sub> = 10 | <img src="girl10_1.png" width="300"> | <img src="girl10_5.png" width="300"> | <img src="girl10_10.png" width="300"> | <img src="girl10_20.png" width="300"> | <img src="girl10_40.png" width="300"> | <img src="girl10_60.png" width="300"> |
| &sigma;<sub>G</sub> = 30 | <img src="girl30_1.png" width="300"> | <img src="girl30_5.png" width="300"> | <img src="girl30_10.png" width="300"> | <img src="girl30_20.png" width="300"> | <img src="girl30_40.png" width="300"> | <img src="girl30_60.png" width="300"> |

Potlačení šumu v obrázku s architekturou níže. V první ukázce &sigma;<sub>G</sub> = 30 a &sigma;<sub>b</sub> = 20. 

    open bilateral/archi.png
    bfilter 30 20 1
    save png bilateral/archi30_20

<img src="archi.png" width="500"> 
<img src="archi30_20.png" width="500"> 

V další ukázce &sigma;<sub>G</sub> = 10 a &sigma;<sub>b</sub> = 40. 

    open bilateral/archi.png
    bfilter 10 40 1
    save png bilateral/archi10_40

<img src="archi.png" width="500"> 
<img src="archi10_40.png" width="500"> 

V ukázce &sigma;<sub>G</sub> = 20 a &sigma;<sub>b</sub> = 40. 

    open bilateral/archi.png
    bfilter 20 40 1
    save png bilateral/archi20_40

<img src="archi.png" width="500"> 
<img src="archi20_40.png" width="500">


V poslední ukázce &sigma;<sub>G</sub> = 10 a &sigma;<sub>b</sub> = 60. 

    open bilateral/archi.png
    bfilter 10 60 1
    save png bilateral/archi10_60

<img src="archi.png" width="500"> 
<img src="archi10_60.png" width="500">


---

## Ad normalize

Zkusil jsem vyhodit normalizaci obou jader a nastavení je skutečně intuitivní, resp. co se jasové složky týče, tak dává smysl (a výsledky odpovídají intuici) použít sigmu pro jádro odpovídající intenzitám pohybující ve stejném rozsahu jako samotné hodnoty. Jelikož interně jsou hodnoty [0, 1], tak dává smysl použít rozptyly z rozsahu [0, 1], filtr produkuje odpovídající výsledky viz níže. 

Také jsem doplnil načítání/zpracování rgb obrázků, spolu s tím jsem chíli experimentoval se vzdálenostmi v CIE-LAB, (místo zpracování po složkách v RGB), ale pak hrozilo, že nad tím strávím více času než by bylo vhodné, tak jsem přešel zpět k RGB. LAB kód jsem ale commitul před smazáním, dá se dohledat. 

Zleva: originál |  &sigma;<sub>G</sub> = 10 a &sigma;<sub>b</sub> = 0.05 |  &sigma;<sub>G</sub> = 10 a &sigma;<sub>b</sub> = 0.1 |  &sigma;<sub>G</sub> = 10 a &sigma;<sub>b</sub> = 0.2 |  &sigma;<sub>G</sub> = 20 a &sigma;<sub>b</sub> = 0.1, všude je použit log

<img src="facet.png" width="200"> 
<img src="f2.png" width="200">
<img src="f1.png" width="200">
<img src="f3.png" width="200">
<img src="f4.png" width="200">


Na závěr, něco se jednou nepovedlo, ale výsledek má taky něco do sebe :)

<img src="wow.png" width="300">
